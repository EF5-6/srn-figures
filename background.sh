#!/bin/zsh

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

# Exports a map background from GRASS GIS.

set -e

# Settings.
# ---------------------------------------------------------
GRASS_VECTOR_MASK="openwater"
GRASS_RASTER_BACKGROUND="elevation_50m_lanczos"
GRASS_RASTER_TEMPORARY="__background"

OUTPUT_IMAGE="external_data/elevation.png"
OUTPUT_METADATA="external_data/elevation.json"

IMAGE_SCALE=0.5

OUTLINE_COLOR="black"
OUTLINE_WIDTH=4

# NOTE: This needs to be synchronized with the colorbar in reconstruction.py.
# TODO: Export these value as metadata.
RASTER_COLORS="
-200 110:174:161
0 143:188:143
600 255:255:0
1200 255:165:0
2000 198:95:71
"
# ---------------------------------------------------------

# Locals.
latitude=$(g.region -p|grep '^cols:'|awk '{print $2}')
longitude=$(g.region -p|grep '^rows:'|awk '{print $2}')
width=$(( latitude * IMAGE_SCALE ))
height=$(( longitude * IMAGE_SCALE ))

# Globals.
export GRASS_RENDER_IMMEDIATE=cairo
export GRASS_RENDER_FILE="${OUTPUT_IMAGE}"
export GRASS_RENDER_WIDTH="${width}"
export GRASS_RENDER_HEIGHT="${height}"
export GRASS_RENDER_TRANSPARENT=TRUE
export GRASS_RENDER_FILE_READ=TRUE

# Pre cleanup.
rm -v -f "${OUTPUT_IMAGE}"

# Set mask.
r.mask -r || true
r.mask -i "${GRASS_VECTOR_MASK}"

# Create maps.
r.mapcalc "${GRASS_RASTER_TEMPORARY} = ${GRASS_RASTER_BACKGROUND}" --overwrite
echo "${RASTER_COLORS}" | r.colors map="${GRASS_RASTER_TEMPORARY}" rules=-

# Draw.
d.rast "${GRASS_RASTER_TEMPORARY}"
d.vect \
    "${GRASS_VECTOR_MASK}" \
    type=boundary \
    color="${OUTLINE_COLOR}" \
    width="${OUTLINE_WIDTH}"

# Unset mask.
r.mask -r

# Post cleanup.
g.remove type="raster" name="${GRASS_RASTER_TEMPORARY}" -f

# Save metadata.
eval $(g.region -g)
echo \
"{
    'north': ${n},
    'south': ${s},
    'east': ${e},
    'west': ${w},
    'nsres': ${nsres},
    'ewres': ${ewres},
    'rows': ${rows},
    'cols': ${cols}
}" | tr "'" '"' > "${OUTPUT_METADATA}"
