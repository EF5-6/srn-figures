#!/usr/bin/env python3
# pylama:ignore=D103,E402,E741

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Produce the "transpositions" figure."""

from itertools import product
from multiprocessing import Pool
from pathlib import Path

import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from common import (
    IS_SITE_KEY,
    compare_networks,
    make_file_names,
    make_roadnet,
    run_generate_or_draw,
    save_figure,
)
from toolbox.comprep import CompositionalRepresentation

DATA_FILE, FIGURE_FILE = make_file_names(__file__)
DATA_FILE_2 = Path.cwd() / "breakpoint.npy"

DATA_ALPHA = 0.5
DATA_NUM_NETS = 1000

FIGURE_HEIGHT = 250
FIGURE_WIDTH = 500
FIGURE_COLORBAR_LENGTH = 0.95
FIGURE_COLORSCALE = "thermal_r"


def _make_and_compare(base_network, alpha, seed, order):
    if order is None:
        return 0.0
    else:
        network = make_roadnet(
            alpha, seed, extract_roads=True, order=tuple(order)
        )

        d = compare_networks(base_network, network)

        return d


def generate():
    # HACK: Get number of sites and connections.
    probe_network = make_roadnet(0, 1)
    probe_sites = tuple(
        v for v, d in probe_network.nodes(data=True) if d[IS_SITE_KEY]
    )
    s = len(probe_sites)
    k = s * (s - 1) // 2

    print(f"  Computing {DATA_NUM_NETS} base networks...")

    # Produce random seeds.
    np.random.seed(1)
    seeds = np.random.randint(2**32 - 1, size=DATA_NUM_NETS)

    # Create DATA_NUM_NETS many random base orders.
    base_orders = []
    for seed in seeds:
        np.random.seed(seed)
        base_order = list(range(k))
        np.random.shuffle(base_order)
        base_orders.append(base_order)

    # Compute a base network from every base order.
    with Pool() as pool:
        base_networks = pool.starmap(
            make_roadnet,
            (
                (DATA_ALPHA, None, True, tuple(base_order))
                for base_order in base_orders
            ),
        )

    # Compare all transpositions with the associated base network.
    D = np.zeros(shape=(k - 1, k - 1))
    for i in range(len(seeds)):
        base_order = base_orders[i]
        base_network = base_networks[i]

        print(
            f"  Evaluating {(k - 1)*(k - 2)//2} variations of base "
            f"network {i + 1}/{DATA_NUM_NETS}..."
        )

        orders = [tuple(base_order)]  # Base to find numeric zero.
        for i in range(k - 1):
            for j in range(k - 1):
                if i < j:
                    order = base_order.copy()
                    order[i], order[j] = order[j], order[i]
                    orders.append(tuple(order))
                else:
                    orders.append(None)

        with Pool() as pool:
            results = pool.starmap(
                _make_and_compare,
                ((base_network, DATA_ALPHA, seed, order) for order in orders),
            )

        zero = results.pop(0)

        # Clip at zero to work around rare numeric issues.
        D += (np.array(results) - zero).clip(0).reshape(D.shape)

    D /= DATA_NUM_NETS
    D += D.T

    np.save(DATA_FILE, D)


def draw():
    if not DATA_FILE.exists():
        raise RuntimeError(f"Missing data file {DATA_FILE}.")

    if not DATA_FILE_2.exists():
        raise RuntimeError(f"Missing data file {DATA_FILE_2}.")

    # Load all transpositions data.
    D = np.load(DATA_FILE)
    max_value = np.max(D)
    D /= max_value

    I = tuple(range(1, D.shape[0] + 1))

    log_D = D.copy()
    log_D[log_D == 0] = float("nan")
    log_D = np.log(log_D)
    log_D[np.isnan(log_D)] = float("-inf")

    # Recover metadata.
    k = D.shape[1] + 1
    s = int((1 + 8 * k) ** 0.5 + 1) // 2
    assert k == s * (s - 1) // 2

    # Load high resolution adjacent transposition data.
    _A = np.load(DATA_FILE_2)
    _alphas = _A[:, 0]
    _alpha_index = int(np.where(_alphas == DATA_ALPHA)[0][0])
    d = _A[_alpha_index, 1:]
    d /= max_value

    # Fit to the adjacent transposition data.
    x = np.array(range(1, k))
    _params = CompositionalRepresentation.from_stats_vector(
        d, return_params=True, breakpoint=s
    )
    _, _, beta, lbd, delta, gamma = _params
    _xe = x[:s]
    _xp = x[s:]
    _fe = beta * np.exp(lbd * _xe)
    _fp = delta * _xp**gamma
    fit = np.concatenate([_fe, _fp])

    # Extrapolate from the fit using a "sum rule".
    F = np.zeros(shape=D.shape)

    for i in range(D.shape[0] - 1):
        F[i, i + 1] = fit[i]

    for i, j in product(*(range(dim) for dim in D.shape)):
        if i + 1 < j:
            F[i, j] = sum(F[k, k + 1] for k in range(i, j))

    F += F.T

    log_F = F.copy()
    log_F[log_F == 0] = float("nan")
    log_F = np.log(log_F)
    log_F[np.isnan(log_F)] = float("-inf")

    # Prepare figure.
    fig = make_subplots(
        cols=2,
        # Spacing nudged to work around an aliasing issue:
        # https://github.com/plotly/plotly.py/issues/3562
        horizontal_spacing=0.0501,
        shared_yaxes=True,
    )

    log_plot = go.Heatmap(
        x=I,
        y=I,
        z=log_D,
        colorbar=dict(thickness=10, len=FIGURE_COLORBAR_LENGTH, dtick=4),
        colorscale=FIGURE_COLORSCALE,
        zmin=-16,
        zmax=0,
    )

    sum_plot = go.Heatmap(
        x=I,
        y=I,
        z=log_F,
        showscale=False,
        showlegend=False,
        colorscale=FIGURE_COLORSCALE,
        zmin=-16,
        zmax=0,
    )

    fig.add_trace(log_plot, col=1, row=1)
    fig.add_trace(sum_plot, col=2, row=1)

    fig.update_xaxes(
        color="black",
        title_font_family="Times New Roman",
        title_font_size=18,
        title_font_color="black",
        tickmode="array",
        tickvals=[1, k - 1],
        ticktext=[1, "k-1"],
        ticks="outside",
    )

    fig.update_xaxes(
        title_text="&nbsp;" * 45 + "First index i of (i, j)-exchange",
        col=1,
        row=1,
    )

    fig.update_yaxes(
        tickmode="array",
        title_font_family="Times New Roman",
        title_font_size=18,
        title_font_color="black",
        color="black",
        title_text="Second index j",
        tickvals=[1, k - 1],
        ticktext=[1, "k-1"],
        col=1,
        row=1,
        ticks="outside",
    )

    fig.update_layout(
        autosize=False,
        height=FIGURE_HEIGHT,
        width=FIGURE_WIDTH,
        margin=dict(b=5, l=5, r=5, t=5),
        paper_bgcolor="white",
        plot_bgcolor="white",
        yaxis1=dict(scaleanchor="x1", constrain="domain"),
        yaxis2=dict(scaleanchor="x2", constrain="domain"),
    )

    fig.add_annotation(
        text="<b>A</b>  Measured",
        xref="paper",
        yref="paper",
        x=0.11,
        y=-0.15,
        showarrow=False,
        font=dict(family="Times New Roman", size=18, color="black"),
    )

    fig.add_annotation(
        text="<b>B</b>  Predicted",
        xref="paper",
        yref="paper",
        x=0.89,
        y=-0.15,
        showarrow=False,
        font=dict(family="Times New Roman", size=18, color="black"),
    )

    save_figure(fig, FIGURE_FILE)


if __name__ == "__main__":
    run_generate_or_draw(generate, draw)
