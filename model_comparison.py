#!/usr/bin/env python3
# pylama:ignore=D103

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Produce the model comparison figures."""

# TODO: Use pandas instead of the dict madness.

import json
import math
import pickle
from functools import lru_cache
from itertools import combinations, product
from multiprocessing import Pool

import networkx as nx
import numpy as np
import plotly.graph_objects as go
import shapely
from more_itertools import pairwise
from plotly.subplots import make_subplots

from common import (
    EXTERNAL_DATA_DIR,
    make_file_names,
    run_generate_or_draw,
    save_figure,
)
from reconstruction import draw_reference, draw_roads, draw_sites, draw_terrain
from toolbox.compare import APSP_SPATIAL_DISTANCE
from webapp.logic import build_roads

DATA_FILE, FIGURE_FILE = make_file_names(__file__)

DATA_NUM_RANDOM = 1000

FIGURE_COMPARISON_HEIGHT = 360
FIGURE_COMPARISON_WIDTH = 500
FIGURE_NETWORKS_9_HEIGHT = 1280
FIGURE_NETWORKS_9_WIDTH = 1000
FIGURE_NETWORKS_4_HEIGHT = 880
FIGURE_NETWORKS_4_WIDTH = 640

IS_SITE_KEY = "is_site"
SITE_ID_KEY = "site_cat"
REF_COST_KEY = "length"
NET_COST_KEY = "cost"
ROAD_TIER_KEY = "road_tier"
EDGE_VERTS_KEY = "_vertices"

SIMPLIFICATION_RESOLUTION = 500
FIT_TO_REFERENCE_BATCH_SIZE = 5
DRAW_NETWORKS_PER_BATCH = 1

(
    MODEL_KNN,
    MODEL_CUTOFF,
    MODEL_EFFICIENCY,
    MODEL_OTHER,
    MODEL_SRN_RANDOM,
    MODEL_SRN_EVIDENCE,
    MODEL_SRN_REFERENCE,
) = range(7)

STYLE = {
    MODEL_SRN_RANDOM: dict(
        name="SRN randomized",
        param_name="α",
        labels={
            0.00: "middle right",
            0.10: "middle right",
            0.15: "middle right",
            0.20: "middle right",
            0.25: "middle right",
            0.30: "middle right",
            0.40: "top right",
            0.50: "top center",
            0.55: "top center",
            0.60: "top center",
            0.65: "top center",
            0.70: "top center",
            0.80: "bottom center",
            0.85: "bottom center",
        },
        color="gray",
        line="dash",
    ),
    MODEL_SRN_EVIDENCE: dict(
        name="SRN fit to evidence",
        param_name="α",
        labels={
            0.30: "bottom center",
            0.40: "bottom center",
            0.50: "bottom center",
            0.55: "bottom center",
            0.60: "bottom center",
            0.70: "bottom center",
        },
        plot_include=(0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7),
        color="black",
    ),
    MODEL_SRN_REFERENCE: dict(
        name="SRN fit to reference",
        param_name="α",
        labels={
            0.00: "top center",
            0.10: "middle left",
            0.15: "middle left",
            0.20: "middle left",
            0.25: "bottom center",
            0.40: "bottom center",
            0.55: "bottom center",
            0.60: "bottom center",
            0.65: "bottom center",
            0.75: "bottom center",
            0.80: "bottom center",
            0.85: "bottom center",
            0.90: "bottom center",
        },
        plot_include=(0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7),
        color="gray",
        line="dot",
    ),
    MODEL_KNN: dict(
        name="k-nearest neighbors",
        param_name="k",
        generate=range(12),
        labels={
            3: "top center",
            4: "top center",
            5: "top center",
            6: "top center",
        },
        plot_include=range(3, 12),
        color="blue",
        symbol=2,
    ),
    MODEL_CUTOFF: dict(
        name="path cost limit",
        param_name="c",
        generate=range(1000, 2000 + 1, 50),
        labels={
            1200: "top center",
            1300: "top center",
            1350: "top center",
            1450: "top center",
            1500: "top center",
            1700: "bottom center",
        },
        plot_include=range(1200, 2000 + 1, 100),
        color="red",
        symbol=2,
    ),
    MODEL_EFFICIENCY: dict(
        name="efficiency network",
        param_name=None,
        param_str_func=lambda x: f"{int((x - 1) * 100)}%",
        generate=list(np.arange(1, 3 + 0.25, 0.25)) + [1.1],
        labels={
            1.10: "middle left",
            1.25: "bottom center",
            1.50: "bottom center",
            2.25: "bottom center",
            2.50: "bottom center",
            2.75: "bottom center",
        },
        plot_include=[1.0, 1.1, 1.25, 1.5, 1.75, 2.0, 2.25, 2.5, 2.75],
        color="orange",
        symbol=2,
    ),
    MODEL_OTHER: dict(
        name="non-parametric",
        param_name=None,
        mode="markers+text",
        labels={
            "MST": "middle left",
            "RNG": "middle left",
            "GG": "middle left",
            "DT": "middle left",
        },
        bold_labels=True,
        plot_include=("MST", "RNG", "GG", "DT"),
        color="orange",
        symbol=2,
        outline=dict(width=1.5, color="black"),
    ),
}

for style in STYLE.values():
    assert set(("name", "param_name", "labels")).issubset(set(style.keys()))
    assert "plot_include" not in style or len(style["plot_include"]) <= 9


def map_site_ids_to_node_numbers(network):
    return {
        d[SITE_ID_KEY]: v
        for v, d in network.nodes(data=True)
        if d[IS_SITE_KEY]
    }


@lru_cache()
def sorted_node_numbers_of_sites(network):
    ids2nodes = map_site_ids_to_node_numbers(network)
    return tuple(ids2nodes[id_] for id_ in sorted(ids2nodes))


def load_networks():
    print("Loading terrain graph and reference network...")

    with open(EXTERNAL_DATA_DIR / "terrain.pickle", "rb") as f:
        terrain = pickle.load(f)

    with open(EXTERNAL_DATA_DIR / "reference.pickle", "rb") as f:
        reference = pickle.load(f)

    terrain_sites = sorted_node_numbers_of_sites(terrain)
    reference_sites = sorted_node_numbers_of_sites(reference)

    assert len(terrain_sites) == len(reference_sites)

    return terrain, terrain_sites, reference, reference_sites


@lru_cache()
def shortest_paths(terrain, terrain_sites):
    dists, paths, order = {}, {}, {}

    for s in terrain_sites:
        d, p = nx.single_source_dijkstra(terrain, s, weight=NET_COST_KEY)
        dists[s] = {t: d[t] for t in terrain_sites if t != s}
        paths[s] = {t: p[t] for t in terrain_sites if t != s}
        order[s] = sorted(dists[s], key=lambda t: dists[s][t])

    return dists, paths, order


def generate_evaluation_networks_srn_random(
    terrain, terrain_sites, reference, reference_sites
):
    dists, _, _ = shortest_paths(terrain, terrain_sites)
    measure = make_measure(reference, reference_sites)

    n = len(terrain_sites)
    k = n * (n - 1) // 2

    alphas = np.arange(0.0, 1.0 + 0.01, 0.05)

    np.random.seed(1)

    orders = {}
    for alpha in alphas:
        orders[alpha] = []
        for _ in range(DATA_NUM_RANDOM):
            order = list(range(k))
            np.random.shuffle(order)
            orders[alpha].append(tuple(order))

    mean_dissimilarities = []
    mean_densities = []
    with Pool() as pool:
        for alpha in alphas:
            print(
                f"Evaluating {DATA_NUM_RANDOM} random networks "
                f"for alpha = {alpha:.2f}..."
            )

            networks = pool.starmap(
                build_roads,
                (
                    (terrain, "all_pairs", order, alpha, None, None)
                    for order in orders[alpha]
                ),
            )

            networks = [
                network.edge_subgraph(
                    (u, v)
                    for u, v, d in network.edges(data=True)
                    if d[ROAD_TIER_KEY]
                ).copy()
                for network in networks
            ]

            densities = pool.map(network_density, networks)

            dissimilarities = pool.starmap(
                measure.__call__,
                (
                    (network, terrain_sites, NET_COST_KEY)
                    for network in networks
                ),
            )

            mean_densities.append(np.mean(densities))
            mean_dissimilarities.append(np.mean(dissimilarities))

    np.save(
        DATA_FILE,
        np.vstack(
            [
                np.array(alphas),
                np.array(mean_densities),
                np.array(mean_dissimilarities),
            ]
        ),
    )


def make_networks_srn_from_plans(
    terrain, subfolders=[""], model=MODEL_SRN_EVIDENCE
):
    print(f"Computing {STYLE[model]['name']} networks...")

    plans = []
    identifiers = []
    for batch_num, subfolder in enumerate(subfolders):
        folder = EXTERNAL_DATA_DIR / "plans" / subfolder
        for plan_file in folder.glob("*.plan.json"):
            with open(plan_file, "r") as f:
                plan = json.load(f)

            order = tuple(plan["order"])
            alpha = plan["alpha"]
            batch = model if len(subfolders) == 1 else (model, batch_num + 1)
            identifier = (batch, round(alpha, 2))

            plans.append((order, alpha))
            identifiers.append(identifier)

    with Pool() as pool:
        networks = pool.starmap(
            build_roads,
            (
                (terrain, "all_pairs", order, alpha, None, None)
                for order, alpha in plans
            ),
        )

    networks = [
        network.edge_subgraph(
            (u, v) for u, v, d in network.edges(data=True) if d[ROAD_TIER_KEY]
        ).copy()
        for network in networks
    ]

    return dict(zip(identifiers, networks))


def make_networks_knn(terrain, terrain_sites):
    print("Computing k-nearest neighbors networks...")
    networks = {}
    _, paths, order = shortest_paths(terrain, terrain_sites)

    for k in STYLE[MODEL_KNN]["generate"]:
        if k >= len(terrain_sites):
            continue

        selected_edges = set()

        for s in terrain_sites:
            selected_targets = order[s][:k]
            selected_paths = {t: paths[s][t] for t in selected_targets}
            selected_edges.update(
                edge
                for path in selected_paths.values()
                for edge in pairwise(path)
            )

        if not selected_edges:
            continue

        network = terrain.edge_subgraph(selected_edges).copy()

        if nx.is_connected(network):
            networks[(MODEL_KNN, k)] = network

    return networks


def make_networks_cutoff(terrain, terrain_sites):
    print("Computing maximum path cost networks...")
    networks = {}
    dists, paths, _ = shortest_paths(terrain, terrain_sites)

    for max_distance in STYLE[MODEL_CUTOFF]["generate"]:
        selected_edges = set()

        for s, t in combinations(terrain_sites, 2):
            if dists[s][t] <= max_distance:
                selected_edges.update(pairwise(paths[s][t]))

        network = terrain.edge_subgraph(selected_edges).copy()

        if not set(terrain_sites).issubset(set(network.nodes)):
            # Work around far-away sites being isolated.
            continue

        if nx.is_connected(network):
            networks[(MODEL_CUTOFF, max_distance)] = network

    return networks


def make_networks_efficiency(terrain, terrain_sites):
    print("Computing efficiency networks...")
    factors = sorted(STYLE[MODEL_EFFICIENCY]["generate"])

    assert factors[0] >= 1

    networks = {}
    dists, paths, _ = shortest_paths(terrain, terrain_sites)
    n = len(terrain_sites)

    G = nx.Graph()
    G.add_weighted_edges_from(
        (u, v, w) for u in dists for v, w in dists[u].items()
    )
    T = nx.minimum_spanning_tree(G)

    assert set(T.nodes) == set(terrain_sites)
    assert len(T.edges) == n - 1

    connected = set()
    selected_edges = set()
    for u, v in T.edges:
        connected.update([(u, v), (v, u)])
        selected_edges.update(pairwise(paths[u][v]))

    last_network = terrain.edge_subgraph(selected_edges).copy()
    last_network_dists, _, _ = shortest_paths(last_network, terrain_sites)
    last_factor = 1.0

    target_factor = factors.pop(0)
    while factors:
        last_network_dists, _, _ = shortest_paths(last_network, terrain_sites)

        best_connection = None
        best_value = float("inf")
        for s, t in combinations(terrain_sites, 2):
            if (s, t) in connected:
                pass

            value = dists[s][t] / last_network_dists[s][t]

            if value < best_value:
                best_connection = (s, t)
                best_value = value

        if best_value >= 1:
            break

        s, t = best_connection
        connected.update([(s, t), (t, s)])
        selected_edges.update(pairwise(paths[s][t]))
        new_network = terrain.edge_subgraph(selected_edges).copy()

        num_connections = len(connected) / 2
        new_factor = num_connections / (n - 1)
        assert new_factor > last_factor

        if last_factor <= target_factor and new_factor > target_factor:
            networks[(MODEL_EFFICIENCY, target_factor)] = last_network
            while new_factor > target_factor and factors:
                target_factor = factors.pop(0)  # Not yet reached factor.

        last_network = new_network
        last_factor = new_factor

    return networks


def _make_network_mst(terrain, terrain_sites):
    print("Computing minimum spanning tree network...")
    dists, paths, _ = shortest_paths(terrain, terrain_sites)

    G = nx.Graph()
    G.add_weighted_edges_from(
        (u, v, w) for u in dists for v, w in dists[u].items()
    )
    T = nx.minimum_spanning_tree(G)

    assert set(T.nodes) == set(terrain_sites)

    selected_edges = set()
    for u, v in T.edges:
        selected_edges.update(pairwise(paths[u][v]))

    network = terrain.edge_subgraph(selected_edges).copy()

    assert set(terrain_sites).issubset(set(network.nodes))
    assert nx.is_connected(network)

    return {"MST": network}


def _make_network_voronoi(terrain, terrain_sites):
    print("Computing Voronoi-based network...")
    _, paths, _ = shortest_paths(terrain, terrain_sites)

    C = nx.voronoi_cells(terrain, terrain_sites, weight=NET_COST_KEY)

    selected_edges = set()
    for u, v in combinations(terrain_sites, 2):
        for a, b in product(C[u], C[v]):
            if (a, b) in terrain.edges:
                selected_edges.update(pairwise(paths[u][v]))

    network = terrain.edge_subgraph(selected_edges).copy()

    assert set(terrain_sites).issubset(set(network.nodes))
    assert nx.is_connected(network)

    return {"DT": network}


def _make_network_gabriel(terrain, terrain_sites):
    print("Computing Gabriel-based network...")
    _, paths, _ = shortest_paths(terrain, terrain_sites)

    C = nx.voronoi_cells(terrain, terrain_sites, weight=NET_COST_KEY)
    R = {member: center for center, members in C.items() for member in members}

    selected_edges = set()
    for u, v in combinations(terrain_sites, 2):
        for a, b in product(C[u], C[v]):
            if (a, b) in terrain.edges:
                P = paths[u][v]
                if len(set(R[w] for w in P)) == 2:
                    selected_edges.update(pairwise(P))

    network = terrain.edge_subgraph(selected_edges).copy()

    assert set(terrain_sites).issubset(set(network.nodes))
    assert nx.is_connected(network)

    return {"GG": network}


def _make_network_rng(terrain, terrain_sites):
    print("Computing relative neighborhood network...")
    dists, paths, _ = shortest_paths(terrain, terrain_sites)

    selected_edges = set()
    for u, v in combinations(terrain_sites, 2):
        d = dists[u][v]

        dominated = False
        for s in terrain_sites:
            if s not in (u, v) and dists[s][u] < d and dists[s][v] < d:
                dominated = True
                break

        if not dominated:
            selected_edges.update(pairwise(paths[u][v]))

    network = terrain.edge_subgraph(selected_edges).copy()

    assert set(terrain_sites).issubset(set(network.nodes))
    assert nx.is_connected(network)

    return {"RNG": network}


def make_networks_other(terrain, terrain_sites):
    networks = {}
    networks.update(_make_network_mst(terrain, terrain_sites))
    networks.update(_make_network_voronoi(terrain, terrain_sites))
    networks.update(_make_network_gabriel(terrain, terrain_sites))
    networks.update(_make_network_rng(terrain, terrain_sites))
    return {(MODEL_OTHER, name): networks[name] for name in networks}


def network_density(network):
    return sum(
        shapely.geometry.LineString(d[EDGE_VERTS_KEY])
        .simplify(tolerance=SIMPLIFICATION_RESOLUTION, preserve_topology=False)
        .length
        for _, _, d in network.edges(data=True)
    )


@lru_cache()
def make_measure(reference, reference_sites):
    return APSP_SPATIAL_DISTANCE(
        method="area",
        resolution=SIMPLIFICATION_RESOLUTION,
        network=reference,
        nodes=reference_sites,
        edge_cost_key=REF_COST_KEY,
    )


def evaluate_networks(networks, terrain_sites, reference, reference_sites):
    measure = make_measure(reference, reference_sites)

    with Pool() as pool:
        print("Computing distances to reference...")

        dissimilarities = pool.starmap(
            measure.__call__,
            (
                (network, terrain_sites, NET_COST_KEY)
                for network in networks.values()
            ),
        )

        densities = pool.map(network_density, networks.values())

    return dict(zip(networks, zip(densities, dissimilarities)))


def draw_networks(networks, model, terrain, reference):
    if isinstance(model, tuple):
        model, batch_num = model

        if batch_num > DRAW_NETWORKS_PER_BATCH:
            return

        batch_str = f"_{batch_num}"
    else:
        batch_str = ""

    style = STYLE[model]
    if "plot_include" not in style:
        return

    with open(EXTERNAL_DATA_DIR / "elevation.json") as f:
        elevation_data = json.load(f)

    sites_trace = draw_sites(terrain)
    reference_trace = draw_reference(reference)
    terrain_trace = draw_terrain(
        terrain, legend=True, color="rgb(230, 230, 230)"
    )

    name = style["name"]
    param_name = style["param_name"]
    params_included = style["plot_include"]

    filename = FIGURE_FILE.parent / (
        f"{FIGURE_FILE.stem}_{name.replace(' ', '_')}"
        f"{batch_str}{FIGURE_FILE.suffix}"
    )

    print(f"Generating {filename}...")

    four = len(params_included) == 4

    if four:
        rows, cols = 2, 2
    else:
        cols = 3
        rows = math.ceil(len(params_included) / cols)

    fig = make_subplots(
        rows=rows,
        cols=cols,
        horizontal_spacing=0.04,
        vertical_spacing=0.02,
    )

    k = 0
    for param, network in sorted(networks.items()):
        if param not in params_included:
            continue

        if "param_str_func" in style:
            param_str = style["param_str_func"](param)
        else:
            param_str = str(param)

        recon_trace = draw_roads(network, use_road_tier_key=False)

        i = k // cols + 1
        j = k % cols + 1

        for trace in (
            terrain_trace,
            reference_trace,
            recon_trace,
            sites_trace,
        ):
            trace["showlegend"] = not k
            fig.add_trace(trace, row=i, col=j)

            fig.update_xaxes(
                showgrid=False,
                zeroline=False,
                showticklabels=False,
                range=(elevation_data["west"], elevation_data["east"]),
                title=dict(
                    text=(
                        f"<b>{chr(ord('A')+k)}</b>  "
                        + (f"{param_name} = " if param_name else "")
                        + param_str
                    ),
                    font=dict(
                        family="Times New Roman", size=18, color="black"
                    ),
                    standoff=0,
                ),
                row=i,
                col=j,
            )

            fig.update_yaxes(
                showgrid=False,
                zeroline=False,
                showticklabels=False,
                range=(elevation_data["south"], elevation_data["north"]),
                scaleanchor=f"x{k + 1}",
                row=i,
                col=j,
            )

        k += 1

    fig.update_layout(
        showlegend=True,
        autosize=False,
        height=FIGURE_NETWORKS_4_HEIGHT if four else FIGURE_NETWORKS_9_HEIGHT,
        width=FIGURE_NETWORKS_4_WIDTH if four else FIGURE_NETWORKS_9_WIDTH,
        margin=dict(b=5, l=5, r=5, t=5),
        paper_bgcolor="white",
        plot_bgcolor="white",
        legend=dict(
            font=dict(family="Times New Roman", size=14, color="black"),
            bgcolor="rgba(0, 0, 0, 0)",
            x=0.42 if four else 0.625,
            y=0.11 if four else 0.070,
        ),
    )

    save_figure(fig, filename)


def draw_results(evaluation, reference):
    print(f"Generating {FIGURE_FILE}...")

    reference_density = network_density(reference)
    max_error = np.max(
        np.array([dissimilarity for _, dissimilarity in evaluation.values()])
    )

    nested_evaluation = {model: {} for model, _ in evaluation}
    for (model, params), result in evaluation.items():
        nested_evaluation[model][params] = result

    fig = go.Figure()
    fig.add_vrect(
        x0=0.9,
        x1=1.1,
        line=dict(color="rgba(0, 0, 0, 0)"),
        fillcolor="rgba(0, 0, 0, 0.05)",
        layer="below",
    )
    fig.add_vline(x=1, line=dict(color="lightgray"), layer="below")

    for model in sorted(nested_evaluation):
        results = dict(sorted(nested_evaluation[model].items()))

        style = STYLE[model]
        name = style["name"]
        param_name = style["param_name"]
        labels = style["labels"]
        bold_labels = "bold_labels" in style and style["bold_labels"]

        x, y = zip(*results.values())
        x = np.array(x) / reference_density
        y = np.array(y) / max_error

        params = list(results.keys())

        text = []
        first_label = True
        for param in params:
            if param in labels:
                if "param_str_func" in style:
                    param_str = style["param_str_func"](param)
                else:
                    param_str = str(param)

                if bold_labels:
                    param_str = f"<b>{param_str}</b>"

                text.append(
                    f"{param_name} = {param_str}"
                    if param_name and first_label
                    else param_str
                )
                first_label = False
            else:
                text.append("")

        textposition = [
            labels[param] if param in labels else "top center"
            for param in params
        ]

        select = x <= 2

        x = x[select]
        y = y[select]
        text = np.array(text)[select]
        textposition = np.array(textposition)[select]

        fig.add_trace(
            go.Scatter(
                x=x,
                y=y,
                mode=(
                    style["mode"] if "mode" in style else "lines+markers+text"
                ),
                line=dict(
                    dash=style["line"] if "line" in style else "solid",
                    color=style["color"] if "color" in style else "black",
                ),
                marker=dict(
                    symbol=style["symbol"] if "symbol" in style else 0,
                    line=(style["outline"] if "outline" in style else {}),
                ),
                text=text,
                textposition=textposition,
                name=name,
            )
        )

    fig.update_xaxes(
        title=dict(
            text="Network density relative to reference",
            font=dict(family="Times New Roman", color="black", size=18),
        ),
        range=[0.325, 2.1],
        tickvals=[0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0],
        gridcolor="rgba(0, 0, 0, 0)",
    )
    fig.update_yaxes(
        title=dict(
            text="Relative distance to reference",
            font=dict(family="Times New Roman", color="black", size=18),
        ),
        gridcolor="rgba(0, 0, 0, 0)",
    )

    fig.update_layout(
        paper_bgcolor="white",
        plot_bgcolor="white",
        autosize=False,
        height=FIGURE_COMPARISON_HEIGHT,
        width=FIGURE_COMPARISON_WIDTH,
        margin=dict(b=30, l=5, r=5, t=5),
        legend=dict(
            font=dict(family="Times New Roman", size=13.5, color="black"),
            bgcolor="rgba(0, 0, 0, 0)",
            x=1.0,
            y=1.0,
            xanchor="right",
        ),
    )

    save_figure(fig, FIGURE_FILE)


def generate():
    terrain, terrain_sites, reference, reference_sites = load_networks()

    generate_evaluation_networks_srn_random(
        terrain, terrain_sites, reference, reference_sites
    )


def draw():
    if not DATA_FILE.exists():
        raise RuntimeError(f"Missing data file {DATA_FILE}.")

    # Load terrain and reference networks.
    terrain, terrain_sites, reference, reference_sites = load_networks()

    # Generate networks to analyze.
    networks = dict()
    networks.update(make_networks_srn_from_plans(terrain))
    networks.update(
        make_networks_srn_from_plans(
            terrain,
            subfolders=[
                f"fit_to_reference_{i}"
                for i in range(1, FIT_TO_REFERENCE_BATCH_SIZE + 1)
            ],
            model=MODEL_SRN_REFERENCE,
        )
    )
    networks.update(make_networks_knn(terrain, terrain_sites))
    networks.update(make_networks_cutoff(terrain, terrain_sites))
    networks.update(make_networks_efficiency(terrain, terrain_sites))
    networks.update(make_networks_other(terrain, terrain_sites))

    # Evaluate the density and closeness to reference of each network.
    evaluation = evaluate_networks(
        networks, terrain_sites, reference, reference_sites
    )

    # Average over networks with equal settings.
    add, remove = dict(), []
    for settings, result in evaluation.items():
        model, param = settings

        if not isinstance(model, tuple):
            continue

        model, _ = model
        batch = (model, param)

        add.setdefault(batch, [])
        add[batch].append(result)

        remove.append(settings)

    for old in remove:
        evaluation.pop(old)

    for batch, results in add.items():
        densities, dissimilarities = zip(*results)
        evaluation[batch] = np.mean(densities), np.mean(dissimilarities)

    # Add cached results concerning the mean SRN with random orders.
    for alpha, density, dissimilarity in np.load(DATA_FILE).T:
        evaluation[(MODEL_SRN_RANDOM, round(alpha, 2))] = (
            density,
            dissimilarity,
        )

    # Draw a figure with all quantitative results.
    draw_results(evaluation, reference)

    # Draw all networks.
    with Pool() as pool:
        nested_networks = {model: {} for model, _ in networks}
        for (model, params), network in networks.items():
            nested_networks[model][params] = network

        pool.starmap(
            draw_networks,
            (
                (nested_networks[model], model, terrain, reference)
                for model in nested_networks
            ),
        )


if __name__ == "__main__":
    run_generate_or_draw(generate, draw)
