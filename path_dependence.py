#!/usr/bin/env python3
# pylama:ignore=D103,E402

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Produce the "path_dependence" figure."""

from multiprocessing import Pool

import numpy as np
import plotly.graph_objects as go
from numpy.linalg import lstsq

from common import (
    ALPHA_DIGITS,
    GRID_COLOR,
    ROAD_COLOR,
    SITE_FILL_COLOR,
    compare_networks,
    make_file_names,
    make_roadnet,
    run_generate_or_draw,
    save_figure,
)
from effect_of_alpha import draw_network

DATA_FILE, FIGURE_FILE = make_file_names(__file__)

DATA_ALPHA_STEP = 1 / 20
DATA_NUM_PAIRS = 5000

FIGURE_HEIGHT = 300
FIGURE_WIDTH = 500
FIGURE_EXAMPLE_ALPHA = 0.4


def generate():
    np.random.seed(1)

    alphas = np.round(
        np.arange(0, 1 + DATA_ALPHA_STEP, DATA_ALPHA_STEP), ALPHA_DIGITS
    )

    results = []
    for alpha in alphas:
        seeds = np.random.randint(2**32 - 1, size=2 * DATA_NUM_PAIRS)

        print(
            f"  Comparing {DATA_NUM_PAIRS} random road network pairs for "
            f"alpha = {alpha:.2f}..."
        )

        with Pool() as pool:
            networks = pool.starmap(
                make_roadnet, ((alpha, seed, True) for seed in seeds)
            )

        nets1 = networks[:DATA_NUM_PAIRS]
        nets2 = networks[DATA_NUM_PAIRS:]

        result = []
        for i, (G, H) in enumerate(zip(nets1, nets2)):
            result.append(compare_networks(G, H))

        results.append(result)

    results = np.array(results)

    np.save(DATA_FILE, np.hstack([alphas[:, np.newaxis], results]))


def draw():
    if not DATA_FILE.exists():
        raise RuntimeError(f"Missing data file {DATA_FILE}.")

    # Load data.
    A = np.load(DATA_FILE)
    alphas = A[:, 0]
    results = A[:, 1:]
    normalized_results = results / np.max(results)
    percentiles = np.percentile(
        normalized_results, [1, 25, 50, 75, 99], axis=1
    )

    # Fit a proportional function to the normalized mean.
    one_minus_alphas = (1 - alphas)[:, np.newaxis]
    means = np.mean(normalized_results, axis=1)
    a, _, _, _ = lstsq(one_minus_alphas, means, rcond=None)
    a = a[0]
    rho = np.corrcoef(means, a * (1 - alphas))[0, 1]
    print(
        f"Proportional relation between 1 - alpha and mean dissimilarity:\n"
        f"slope a = {a} (fixed intersect b = 0)\n"
        f"Pearson correlation rho = {rho}"
    )

    # Create and plot networks for inset.
    networks = draw_network(
        make_roadnet(alpha=FIGURE_EXAMPLE_ALPHA), unused=False, sites=False
    ) + draw_network(
        make_roadnet(alpha=FIGURE_EXAMPLE_ALPHA, road_seed=2),
        unused=False,
        sites=False,
    )

    # Assign networks to axes.
    for i, network in enumerate(networks):
        network.update(xaxis=f"x{i+2}", yaxis=f"y{i+2}")

    # Make the boxplot.
    fig = go.Figure()
    fig.add_trace(
        go.Box(
            x=alphas,
            lowerfence=percentiles[0],
            q1=percentiles[1],
            median=percentiles[2],
            q3=percentiles[3],
            upperfence=percentiles[4],
            line_color=ROAD_COLOR,
            fillcolor=SITE_FILL_COLOR,
            whiskerwidth=0.5,
        )
    )

    fig.update_xaxes(
        title=dict(
            text="Parameter α",
            standoff=12,
            font=dict(family="Times New Roman", size=18, color="black"),
        ),
        dtick=0.05,
        tickformat=".2f",
        color="black",
    )

    fig.update_yaxes(
        title=dict(
            text="Average network dissimilarity",
            font=dict(family="Times New Roman", size=18, color="black"),
        ),
        range=(0 - 1e-2, 0.8 + 1e-2),
        domain=(0, 1),
        tickformat=".1f",
        dtick=0.2,
        gridcolor=GRID_COLOR,
        zeroline=False,
        color="black",
    )

    # Add networks inset.
    for network in networks:
        fig.add_trace(network)

    y0, y1 = 0.505, 0.985
    x0, x1, x2, x3 = 0.25, 0.55, 0.7, 1.0
    ym = y0 + (y1 - y0) / 2
    xm = x1 + (x2 - x1) / 2

    arrow = dict(
        x=x2 - 0.01,
        y=ym,
        xref="paper",
        yref="paper",
        showarrow=True,
        ax=-62,
        ay=0,
        arrowhead=1,
        arrowsize=1,
        arrowwidth=1.5,
        arrowcolor="black",
    )

    arrow_label = dict(
        x=xm,
        y=ym,
        xref="paper",
        yref="paper",
        text="Shuffle π",
        font=dict(color="black", family="Times New Roman", size=13),
        yanchor="bottom",
        xshift=-2.5,
        yshift=-1,
        showarrow=False,
    )

    inset_gap_closer = dict(
        type="rect",
        xref="paper",
        yref="paper",
        x0=x1,
        x1=x2,
        y0=y0,
        y1=y1,
        fillcolor="white",
        layer="above",
        line_width=0,
    )

    inset_xaxes = [
        dict(
            domain=domain,
            showgrid=False,
            zeroline=False,
            showticklabels=False,
        )
        for domain in ([x0, x1], [x2, x3])
    ]

    inset_yaxes = [
        dict(
            domain=[y0, y1],
            showgrid=False,
            zeroline=False,
            showticklabels=False,
            scaleanchor=f"x{num}",
        )
        for num in range(2, 4)
    ]

    fig.update_layout(
        autosize=False,
        height=FIGURE_HEIGHT,
        width=FIGURE_WIDTH,
        margin=dict(b=5, l=5, r=5, t=5),
        showlegend=False,
        paper_bgcolor="white",
        plot_bgcolor="white",
        xaxis2=inset_xaxes[0],
        xaxis3=inset_xaxes[1],
        yaxis2=inset_yaxes[0],
        yaxis3=inset_yaxes[1],
        shapes=[inset_gap_closer],
        annotations=[arrow, arrow_label],
    )

    save_figure(fig, FIGURE_FILE)


if __name__ == "__main__":
    run_generate_or_draw(generate, draw)
