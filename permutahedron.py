#!/usr/bin/env python3
# pylama:ignore=D103,E402

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Produce the "permutahedron" figure."""

from itertools import combinations, permutations
from pathlib import Path

import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from common import GRID_COLOR, make_file_names, save_figure
from toolbox.comprep import CompositionalRepresentation
from toolbox.population import Plan

_, FIGURE_FILE = make_file_names(__file__)
DATA_FILE = Path.cwd() / "breakpoint.npy"

DATA_ALPHA = 0.5

FIGURE_HEIGHT = 300
FIGURE_WIDTH = 500
FIGURE_COLORS = ["blue", "red", "orange"]


def sphere_trace(func=None, *, resolution=100):
    """Draw a sphere."""
    theta = np.linspace(0, 2 * np.pi, resolution)
    phi = np.linspace(0, np.pi, resolution)

    x = np.outer(np.cos(theta), np.sin(phi))
    y = np.outer(np.sin(theta), np.sin(phi))
    z = np.outer(np.ones(resolution), np.cos(phi))

    trace = go.Surface(
        x=x,
        y=y,
        z=z,
        colorscale=[[0, "lightgrey"], [1, "lightgrey"]],
        opacity=0.3,
        showscale=False,
    )

    return trace


def draw():
    if not DATA_FILE.exists():
        raise RuntimeError(f"Missing data file {DATA_FILE}.")

    # We can visualize at most a transformation of S_4 (into R^3).
    n, n2 = 4, 11

    # Load data.
    A = np.load(DATA_FILE)
    alphas = A[:, 0]
    D = A[:, 1:]

    # Make sure the data contains statistics for the chosen alpha.
    if DATA_ALPHA not in alphas:
        raise RuntimeError(f"Data does not cover alpha = {DATA_ALPHA}.")

    # Extract the desired Euclidean distances from the data.
    alpha_index = int(np.where(alphas == DATA_ALPHA)[0][0])
    d = D[alpha_index, :]
    d /= np.max(d)

    # Generate dummy plans representing the permutations S_n.
    S = tuple(permutations(range(n)))
    plans = tuple(Plan(pi, None) for pi in S)

    # Apply the Euclidean transformation from S_n to R^(n-1).
    comprep = CompositionalRepresentation.from_aitchison_distances(
        d[: n - 1], normalize=False
    )
    ilr = np.array([comprep.ilr(plan) for plan in plans]).T
    ilr /= np.linalg.norm(ilr, axis=0)  # Normalize onto unit sphere.

    # Setup a figure and draw the sphere.
    fig = make_subplots(
        rows=1,
        cols=2,
        horizontal_spacing=0.05,
        column_widths=[0.7, 0.3],
        specs=[[dict(type="scene"), dict(type="xy")]],
    )
    fig.add_trace(sphere_trace(), row=1, col=1)

    # Draw adjacent transposition edges in 3D.
    for (pi1, x1), (pi2, x2) in combinations(zip(S, ilr[: n - 1].T), 2):
        mismatch = [int(pi1[i] != pi2[i]) for i in range(n)]

        if sum(mismatch) != 2:
            continue

        fst = mismatch.index(1)
        mismatch[fst] = 0
        scd = mismatch.index(1)

        x, y, z = np.vstack([x1, x2]).T

        if abs(fst - scd) == 1:
            color = FIGURE_COLORS[fst]

            fig.add_trace(
                go.Scatter3d(
                    x=x,
                    y=y,
                    z=z,
                    mode="lines",
                    line=dict(color=color, width=4),
                    showlegend=False,
                    name=f"({fst + 1}, {scd + 1})",
                ),
                row=1,
                col=1,
            )

    # Draw permutation markers in 3D.
    x, y, z = ilr + 0.01
    fig.add_trace(
        go.Scatter3d(
            x=x,
            y=y,
            z=z,
            mode="markers",
            showlegend=False,
            marker=dict(color="black", size=4),
        ),
        row=1,
        col=1,
    )

    # Draw adjacent transposition edges and permutation markers in 2D.
    for i, l in enumerate(d[:n2]):
        fig.add_trace(
            go.Scatter(
                x=(i + 1, i + 1),
                y=(0, l),
                mode="lines+markers",
                marker=dict(color="black" if i < 3 else "grey", size=6),
                line=dict(
                    color=FIGURE_COLORS[i] if i < 3 else "lightgrey", width=3
                ),
                showlegend=False,
                xaxis="x2",
                yaxis="y2",
            ),
            row=1,
            col=2,
        )

    # Prepare permutation annotations.
    labels = ["".join(np.array(["D", "B", "C", "A"])[list(s)]) for s in S]
    xshift = 6
    yshift = 11
    shifts = dict(
        ABDC=(0, +yshift),
        ADBC=(0, +yshift),
        ADCB=(0, +yshift),
        ACDB=(0, +yshift),
        ABCD=(-xshift, -yshift),
        ACBD=(-xshift, -yshift),
        BACD=(-xshift, -yshift),
        BCAD=(-xshift, -yshift),
        CBAD=(-xshift, -yshift),
        CABD=(-xshift, -yshift),
    )

    # Assemble permutation annotations.
    scene_annotations = []
    for label, coords in zip(labels, ilr.T):
        if label not in shifts:
            continue

        x, y, z = coords
        xs, ys = shifts[label]

        scene_annotations.append(
            dict(
                x=x,
                y=y,
                z=z,
                xshift=xs,
                yshift=ys,
                text=label,
                font=dict(color="black", size=10),
                showarrow=False,
                bgcolor="orange",
                opacity=0.8,
            )
        )

    scene_axes = dict(
        showbackground=False,
        gridcolor=GRID_COLOR,
        zeroline=False,
        tickvals=[-1, 0, 1],
        range=[-1, 1],
    )

    fig.update_layout(
        scene=dict(
            xaxis=scene_axes,
            yaxis=scene_axes,
            zaxis=scene_axes,
            annotations=scene_annotations,
        ),
        scene_camera=dict(
            up=dict(x=0, y=0, z=1),
            center=dict(x=0, y=0, z=-0.15),
            eye=dict(x=1.25, y=1.25, z=1.0),
        ),
        autosize=False,
        height=FIGURE_HEIGHT,
        width=FIGURE_WIDTH,
        margin=dict(b=0, l=0, r=0, t=0),
        paper_bgcolor="white",
        plot_bgcolor="white",
        xaxis=dict(
            tickvals=tuple(range(1, n2 + 1)),
            ticktext=[f"({i:d}, {i+1:d})" for i in range(1, n2 + 1)],
            tickangle=90,
            title=dict(
                text="Positions exchanged<br />in construction order",
                font=dict(family="Times New Roman", color="black", size=15),
            ),
            title_standoff=0,
        ),
        yaxis=dict(
            # HACK: The lower bound somehow allows the left subfigure to extend
            #       below the x axis of the right subfigure, while not actually
            #       moving the right subfigure further up.
            domain=[0.28, 1.0],
            tickvals=[0, 1],
            range=[-0.05, 1.05],
            title=dict(
                text="Distance in embedding",
                font=dict(family="Times New Roman", color="black", size=15),
            ),
            title_standoff=0,
        ),
    )

    save_figure(fig, FIGURE_FILE)


if __name__ == "__main__":
    draw()
