#!/usr/bin/env python3
# pylama:ignore=D103,E402,E741

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Produce the "reconstruction_more" figure."""

import json
import pickle

from plotly.subplots import make_subplots

from common import EXTERNAL_DATA_DIR, make_file_names, save_figure
from reconstruction import (
    draw_milestones,
    draw_reference,
    draw_roads,
    draw_sites,
    draw_stations,
    draw_terrain,
)
from webapp.logic import build_roads

_, FIGURE_FILE = make_file_names(__file__)

FIGURE_HEIGHT = 1280
FIGURE_WIDTH = 1000


def draw():
    # Load external data.
    with open(EXTERNAL_DATA_DIR / "terrain.pickle", "rb") as f:
        terrain = pickle.load(f)

    with open(EXTERNAL_DATA_DIR / "reference.pickle", "rb") as f:
        reference = pickle.load(f)

    with open(EXTERNAL_DATA_DIR / "elevation.json") as f:
        elevation_data = json.load(f)

    with open(EXTERNAL_DATA_DIR / "stations.json") as f:
        stations = json.load(f)

    with open(EXTERNAL_DATA_DIR / "milestones.json") as f:
        milestones = json.load(f)

    # Create shared traces.
    sites_trace = draw_sites(terrain)
    stations_trace = draw_stations(stations)
    milestones_trace = draw_milestones(milestones)
    reference_trace = draw_reference(reference)
    terrain_trace = draw_terrain(
        terrain, legend=True, color="rgb(230, 230, 230)"
    )

    # Setup figure.
    fig = make_subplots(
        rows=3, cols=3, horizontal_spacing=0.04, vertical_spacing=0.02
    )

    # Plot subfigures.
    for k, plan in enumerate(
        sorted((EXTERNAL_DATA_DIR / "plans").glob("*.plan.json"))
    ):
        print(f"Drawing {plan}...")

        with open(plan, "r") as f:
            plan = json.load(f)

        alpha = plan["alpha"]
        order = tuple(plan["order"])

        reconstruction = build_roads(
            network=terrain,
            mode="all_pairs",
            alpha=alpha,
            order=order,
            seed=None,
            selected_site=None,
        )

        recon_trace = draw_roads(reconstruction)

        if k:
            recon_trace["showlegend"] = False

        i = k // 3 + 1
        j = k % 3 + 1

        for trace in (
            terrain_trace,
            reference_trace,
            recon_trace,
            milestones_trace,
            stations_trace,
            sites_trace,
        ):
            fig.add_trace(trace, row=i, col=j)
            trace["showlegend"] = False

            fig.update_xaxes(
                showgrid=False,
                zeroline=False,
                showticklabels=False,
                range=(elevation_data["west"], elevation_data["east"]),
                title=dict(
                    text=f"<b>{chr(ord('A')+k)}</b>  α = {alpha:.2f}",
                    font=dict(
                        family="Times New Roman", size=18, color="black"
                    ),
                    standoff=0,
                ),
                row=i,
                col=j,
            )

            fig.update_yaxes(
                showgrid=False,
                zeroline=False,
                showticklabels=False,
                range=(elevation_data["south"], elevation_data["north"]),
                scaleanchor=f"x{k + 1}",
                row=i,
                col=j,
            )

    print("Saving figure...")

    # Finish the layout.
    fig.update_layout(
        showlegend=True,
        autosize=False,
        height=FIGURE_HEIGHT,
        width=FIGURE_WIDTH,
        margin=dict(b=5, l=5, r=5, t=5),
        paper_bgcolor="white",
        plot_bgcolor="white",
        legend=dict(
            font=dict(family="Times New Roman", size=14, color="black"),
            bgcolor="rgba(0, 0, 0, 0)",
            x=0.625,
            y=0.070,
        ),
    )

    save_figure(fig, FIGURE_FILE)


if __name__ == "__main__":
    draw()
