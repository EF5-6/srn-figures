#!/usr/bin/env python3
# pylama:ignore=D103,E402,E741

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Generate extended sensitivity/breakpoint statistics."""

import math
import pickle
from collections.abc import Sequence
from copy import copy as shallow_copy
from functools import lru_cache
from itertools import combinations
from multiprocessing import Pool, cpu_count
from pathlib import Path
from typing import Optional, Union

import numpy as np
import plotly.graph_objects as go
import rustworkx as rx
import scipy.spatial
import scipy.stats
import similaritymeasures
from more_itertools import batched, pairwise
from numpy.typing import NDArray
from plotly.subplots import make_subplots
from tqdm import tqdm

from common import (
    GRID_COLOR,
    ROAD_COLOR,
    SITE_BORDER_COLOR,
    SITE_FILL_COLOR,
    padded_range,
    save_figure,
)
from webapp.config import (
    BASE_COST_KEY,
    COORDS_KEY,
    IS_SITE_KEY,
    RDCD_COST_KEY,
    ROAD_TIER_KEY,
)

# -- Settings -----------------------------------------------------------------


# Data generation settings.
ALPHA = 0.5
RESOLUTION = 30  # Roughly the square root of the number of terrain edges.
DATA_NUM_NETS = 20000

# Analysis settings.
BREAKPOINT_FUNCTION = lambda s: s

# Plot settings.
HEIGHT = 1280
WIDTH = 1000
EXPERIMENTS = (1, 2, 3, 4)  # See run_experiment().


# -- Type definitions ---------------------------------------------------------


NestedTupleMatrix = tuple[tuple[float, float]]
ConnectionSequence = Sequence[tuple[int, int]]

# The following are hints towards the network stage that a function expects.
Terrain = rx.PyGraph  # Weighted graph.
Scenario = rx.PyGraph  # Weighted graph with designated sites.
Outcome = rx.PyGraph  # Weighted graph with sites and a road network.
Roadnet = rx.PyGraph  # The road network subgraph of an outcome.


# -- Network creation ---------------------------------------------------------


def make_terrain_delaunay(n: int = RESOLUTION, seed: int = 1):
    np.random.seed(seed)

    points = np.random.random((n**2, 2))
    triangulation = scipy.spatial.Delaunay(points)
    P = triangulation.points

    terrain = rx.PyGraph(multigraph=False)
    terrain.add_nodes_from(tuple({COORDS_KEY: p} for p in P))

    for path in triangulation.simplices:
        terrain.add_edges_from(
            tuple(
                (u, v, {BASE_COST_KEY: np.linalg.norm(P[u] - P[v])})
                for u, v in pairwise(path)
            )
        )

    return terrain


def make_terrain_complete(n: int = RESOLUTION, seed: int = 1):
    np.random.seed(seed)

    P = np.random.random((n, 2))

    terrain = rx.PyGraph(multigraph=False)
    terrain.add_nodes_from(tuple({COORDS_KEY: p} for p in P))
    terrain.add_edges_from(
        tuple(
            (u, v, {BASE_COST_KEY: np.linalg.norm(P[u] - P[v])})
            for u, v in combinations(range(len(P)), 2)
        )
    )

    return terrain


def _mark_sites(terrain: Terrain, sites):
    sites_left = set(sites)
    for v, d in enumerate(terrain.nodes()):
        if v in sites_left:
            d[IS_SITE_KEY] = True
            sites_left.remove(v)
        else:
            d[IS_SITE_KEY] = False


def place_sites(terrain: Terrain, count: int = 20, seed: int = 1):
    np.random.seed(seed)

    n = len(terrain)

    assert count <= n
    sites = tuple(np.random.choice(n, count, False))

    _mark_sites(terrain, sites)

    return sites


def place_sites_clustered(
    terrain: Terrain, count: int = 20, strength=0.5, seed: int = 1
):
    np.random.seed(seed)

    allowed_sites = tuple(
        v
        for v in terrain.node_indices()
        if 2 * min(abs(terrain.get_node_data(v)[COORDS_KEY] - 0.5)) > strength
    )

    assert count <= len(allowed_sites)
    sites = tuple(np.random.choice(allowed_sites, count, False))

    _mark_sites(terrain, sites)

    return sites


def make_scenario_regular(n: int = RESOLUTION, s: int = 16, seed: int = 1):
    NOISE = 1e-4

    # Number of blocks with sites in the corners.
    b = round(s**0.5) - 1
    assert (b + 1) ** 2 == s

    # Side length of each block.
    l = RESOLUTION // 2

    # Side length of the grid.
    r = b * l + 1

    terrain = rx.generators.grid_graph(
        rows=r,
        cols=r,
        weights=[{COORDS_KEY: (i, j)} for i in range(r) for j in range(r)],
        multigraph=False,
    )

    for e in terrain.edge_indices():
        terrain.update_edge_by_index(
            e, {BASE_COST_KEY: 1 + NOISE * np.random.rand()}
        )

    sites = []
    for i in range(0, r, r // b):
        for j in range(0, r, r // b):
            sites.append(i * r + j)
    sites = sites[:s]

    _mark_sites(terrain, sites)

    return terrain, sites


def cost_function(base: float, tier: int, alpha: float):
    """Compute travel cost based on base cost and road tier."""
    if tier > 0:
        return alpha * base
    else:
        return base


def copy_network(network: rx.PyGraph):
    """Make a copy of the network with a shallow copy of node and edge data."""
    copy = rx.PyGraph()

    for d in network.nodes():
        copy.add_node(shallow_copy(d))

    for u, v in network.edge_list():
        copy.add_edge(u, v, shallow_copy(network.get_edge_data(u, v)))

    return copy


def build_roads(
    terrain: Terrain, connections: ConnectionSequence, alpha: float
) -> Outcome:
    network = copy_network(terrain)

    for d in network.edges():
        d[ROAD_TIER_KEY] = 0
        d[RDCD_COST_KEY] = cost_function(d[BASE_COST_KEY], 0, alpha)

    for u, v in connections:
        path = rx.dijkstra_shortest_paths(
            network, u, v, lambda d: d[RDCD_COST_KEY]
        )[v]

        for a, b in pairwise(path):
            d = network.get_edge_data(a, b)
            d[ROAD_TIER_KEY] += 1
            d[RDCD_COST_KEY] = cost_function(
                d[BASE_COST_KEY], d[ROAD_TIER_KEY], alpha
            )

    return network


def extract_roads(network: Outcome) -> Roadnet:
    positive_edges = [
        network.get_edge_endpoints_by_index(e)
        for e in network.edge_indices()
        if network.get_edge_data_by_index(e)[ROAD_TIER_KEY]
    ]
    return network.edge_subgraph(positive_edges)


# -- Network comparison -------------------------------------------------------


@lru_cache()
def _get_sites(network: Scenario) -> Sequence[int]:
    return tuple(
        sorted(
            v
            for v in network.node_indices()
            if network.get_node_data(v)[IS_SITE_KEY]
        )
    )


@lru_cache()
def _get_shortest_paths(network: Scenario):
    sites = _get_sites(network)

    trees = {}
    for u in sites[:-1]:
        trees[u] = rx.dijkstra_shortest_paths(
            network, u, None, lambda d: d[BASE_COST_KEY]
        )

    paths = {}
    for u, v in pairwise(sites):
        paths[u, v] = np.array(
            [network.get_node_data(w)[COORDS_KEY] for w in trees[u][v]]
        )

    return paths


def _array_to_tuple(array: NDArray[np.floating]) -> NestedTupleMatrix:
    return tuple(tuple(x) for x in array)


@lru_cache()
def _compare_paths(P: NestedTupleMatrix, Q: NestedTupleMatrix):
    P, Q = np.array(P), np.array(Q)
    assert np.all(P[0, :] == Q[0, :]) and np.all(P[-1, :] == Q[-1, :])

    if P.shape == Q.shape and np.all(P == Q):
        return 0.0

    return similaritymeasures.area_between_two_curves(P, Q)


def compare_networks(G: Roadnet, H: Roadnet):
    if G.edge_list() == H.edge_list():
        return 0.0

    G_paths = _get_shortest_paths(G)
    H_paths = _get_shortest_paths(H)

    assert G_paths.keys() == H_paths.keys(), (
        list(G_paths.keys()),
        list(H_paths.keys()),
    )

    error = 0.0

    for P, Q in zip(G_paths.values(), H_paths.values()):
        error += _compare_paths(_array_to_tuple(P), _array_to_tuple(Q))

    return error / len(G_paths)


# -- Statistics generation ----------------------------------------------------


def _get_connections(network: Scenario) -> ConnectionSequence:
    # NOTE: This intentionally returns a non-cached mutable.
    sites = _get_sites(network)
    return list(combinations(sites, 2))


def _analyze_batch(
    network: Scenario, base_con_order: ConnectionSequence, alpha: float
):
    k = len(base_con_order)

    # Apply identity and all adjacent transpositions to the base order.
    con_orders = [tuple(base_con_order)]
    for i in range(k - 1):
        con_order = base_con_order.copy()
        con_order[i], con_order[i + 1] = con_order[i + 1], con_order[i]
        con_orders.append(tuple(con_order))

    # Compute all roadnets.
    roadnets = [
        extract_roads(build_roads(network, con_order, alpha))
        for con_order in con_orders
    ]

    # Compare adjacent transposition roadnets to the identity one.
    d = np.zeros(k)
    base_roadnet = roadnets[0]
    for i, roadnet in enumerate(roadnets):
        d[i] = compare_networks(base_roadnet, roadnet)

    assert d[0] == 0.0
    return d[1:]


def analyze(
    experiment_number: int, network: Scenario, samples: int, alpha: float
):
    np.random.seed(1)

    seeds = np.random.randint(2**32 - 1, size=samples)

    # Create random base connection orders.
    base_con_orders = []
    for seed in seeds:
        np.random.seed(seed)

        con_order = _get_connections(network)
        np.random.shuffle(con_order)
        base_con_orders.append(con_order)

    # Analyze sensitivity for every base order.
    with Pool() as pool:
        results = []
        for chunk in batched(
            tqdm(
                base_con_orders,
                f"experiment {experiment_number}",
                unit="sample",
            ),
            cpu_count(),
        ):
            results.extend(
                pool.starmap(
                    _analyze_batch,
                    (
                        (network, base_con_order, alpha)
                        for base_con_order in chunk
                    ),
                )
            )

    # Average results.
    D = np.vstack(results)

    return D


# -- Experiments --------------------------------------------------------------


def run_experiment(experiment: int, samples: int):
    np.random.seed(1)

    if experiment == 1:
        G = make_terrain_delaunay()
        S = place_sites(G)
    elif experiment == 2:
        G = make_terrain_delaunay()
        S = place_sites_clustered(G)
    elif experiment == 3:
        G, S = make_scenario_regular()
    elif experiment == 4:
        G = make_terrain_complete()
        S = place_sites(G, RESOLUTION)
        assert len(G) == len(S)
    else:
        raise ValueError("Undefined experiment.")

    # Optional: Build a random road network for visualization.
    K = list(combinations(S, 2))
    np.random.shuffle(K)
    G = build_roads(G, K, ALPHA)

    # Compute dissimilarity statistics.
    D = analyze(experiment, G, samples, ALPHA)

    return G, D


def _make_filename(experiment: int, samples: int):
    return (
        f"sensitivity.e{experiment}_a{ALPHA}"
        f"_r{RESOLUTION}_s{samples}.pickle"
    )


def generate(experiment: int, samples: int):
    filename = _make_filename(experiment, samples)

    if not Path(filename).exists():
        data = run_experiment(experiment, samples)

        with open(filename, "wb") as f:
            pickle.dump(data, f)

    with open(filename, "rb") as f:
        G, D = pickle.load(f)

    return G, D


# -- Plotting -----------------------------------------------------------------


@lru_cache()
def _edge_coords(network: Outcome, roads_only: bool = False):
    x, y = [], []
    for u, v in network.edge_list():
        if roads_only and not network.get_edge_data(u, v)[ROAD_TIER_KEY]:
            continue
        ux, uy = network.get_node_data(u)[COORDS_KEY]
        vx, vy = network.get_node_data(v)[COORDS_KEY]
        x.extend((ux, vx, None))
        y.extend((uy, vy, None))
    return tuple(x), tuple(y)


def draw_network(network: Union[Scenario, Outcome]):
    traces = []

    # Draw terrain.
    x, y = _edge_coords(network)
    traces.append(
        go.Scatter(
            mode="lines",
            x=x,
            y=y,
            line_color="darkgray",
            line_width=0.5,
            showlegend=False,
        )
    )

    # Draw roads.
    try:
        x, y = _edge_coords(network, roads_only=True)
        traces.append(
            go.Scatter(
                mode="lines",
                x=x,
                y=y,
                line_color=ROAD_COLOR,
                line_width=1.5,
                showlegend=False,
            )
        )
    except KeyError as error:
        if ROAD_TIER_KEY in str(error):  # No roads built yet.
            pass
        else:
            raise

    # Draw sites.
    site_coords = [], []

    for d in network.nodes():
        if not d[IS_SITE_KEY]:
            continue

        x, y = d[COORDS_KEY]
        site_coords[0].append(x)
        site_coords[1].append(y)

    traces.append(
        go.Scatter(
            mode="markers",
            x=site_coords[0],
            y=site_coords[1],
            marker=dict(
                line_width=1.5,
                line_color=SITE_BORDER_COLOR,
                color=SITE_FILL_COLOR,
            ),
            showlegend=False,
        )
    )

    return traces


def numerically_nonzero(
    array: NDArray[np.floating],
    abs_tol: Optional[float] = 1e-16,
    rel_tol: Optional[float] = 1e-8,
):
    """Find numeric non-zero positions in an array.

    :param float or None abs_tol:
        The absolute value of a numeric zero must be smaller than this.

    :param float or None rel_tol:
        The absolute value of a numeric zero must be at most this times the
        next larger absolute value in the array.

    If both tolerances are given, a value must exceed both to be considered
    numerically zero.
    """
    if abs_tol is None and rel_tol is None:
        raise ValueError("Must give at least one tolerance.")

    nz = np.nonzero(array)
    absolute = np.abs(array)
    absolute_values = sorted(set(absolute[nz]))

    largest_numeric_zero = 0
    for x, y in pairwise(absolute_values):
        assert 0 < x < y

        if abs_tol is not None and x > abs_tol:
            break

        if rel_tol is None or x / y < rel_tol:
            assert x > largest_numeric_zero
            largest_numeric_zero = x

    return np.where(absolute > largest_numeric_zero)


def draw_exppow_regime(D: NDArray[np.floating], breakpoint_func, first: bool):
    traces = []

    d = np.mean(D, axis=0)
    d /= np.max(d)

    k = len(d) + 1  # Number of connections.
    s = int((1 + 8 * k) ** 0.5 + 1) // 2  # Number of sites.
    assert math.comb(s, 2) == k
    h = breakpoint_func(s)

    x = np.arange(1, k + 1)

    # Select regime to analyze.
    if first:
        x = x[:h]
        y = d[:h]
    else:
        x = np.log(x[h:])
        y = d[h:]

    # Filter zero measurements (no change observed) before applying log().
    # HACK: Work around the dissimilarity measure sometimes returning numeric
    #       zeros, which can cause extreme outliers.
    nz = numerically_nonzero(y)
    x, y = x[nz], y[nz]
    y = np.log(y)

    # Perform linear regression in logarithmic or log-log space.
    result = scipy.stats.linregress(x, y)
    a = result.slope
    b = result.intercept
    r = result.rvalue
    p = result.pvalue
    y_fit = a * x + b

    traces.append(
        go.Scatter(
            mode="markers",
            x=x,
            y=y,
            marker=dict(symbol=100, color="black"),
            showlegend=False,
        )
    )

    traces.append(
        go.Scatter(
            mode="lines",
            x=x,
            y=y_fit,
            line=dict(color="blue"),
            showlegend=False,
        )
    )

    return traces, dict(r=r, p=p, h=h, s=s)


def draw(breakpoint_func, outfile):
    fig = make_subplots(rows=len(EXPERIMENTS), cols=3)
    letters = " ABCDEFGHIJKL"
    prime = "'"

    for row, experiment in enumerate(EXPERIMENTS, 1):
        network, D = generate(experiment, DATA_NUM_NETS)
        network_traces = draw_network(network)
        exp_traces, exp_meta = draw_exppow_regime(D, breakpoint_func, True)
        pow_traces, pow_meta = draw_exppow_regime(D, breakpoint_func, False)

        fig.add_traces(network_traces, rows=row, cols=1)

        fig.update_xaxes(
            row=row,
            col=1,
            title=dict(
                text=f"<b>{letters[row]}</b>",
                font=dict(family="Times New Roman", size=18, color="black"),
            ),
            color="rgba(0,0,0,0)",  # HACK: This aligns x axis labels.
        )

        fig.update_yaxes(
            row=row,
            col=1,
            showgrid=False,
            zeroline=False,
            showticklabels=False,
            scaleanchor=f"x{3*(row - 1) + 1}",
        )

        for fit_traces, fit_meta, col in zip(
            (exp_traces, pow_traces), (exp_meta, pow_meta), (2, 3)
        ):
            is_exp = fit_traces is exp_traces
            h, s = fit_meta["h"], fit_meta["s"]
            k = s * (s - 1) // 2

            fig.add_traces(fit_traces, rows=row, cols=col)

            fig.update_xaxes(
                row=row,
                col=col,
                title=dict(
                    text=(
                        f"<b>{letters[row]}{prime*(col - 1)}</b>  "
                        f"{'i' if is_exp else 'log i'}    "
                        f"(r² = {fit_meta['r']**2:.2f})"
                    ),
                    font=dict(
                        family="Times New Roman", size=18, color="black"
                    ),
                ),
                color="black",
                gridcolor=GRID_COLOR,
                zeroline=False,
            )

            if is_exp:
                fig.update_xaxes(
                    row=row,
                    col=col,
                    tickvals=[1, s // 2, s],
                    ticktext=[1, "s/2", "s"],
                    range=padded_range(1, h, 0.01),
                )
            else:
                fig.update_xaxes(
                    row=row,
                    col=col,
                    tickvals=[
                        np.log(s),
                        np.log(2 * s),
                        np.log(k / 2),
                        np.log(k - 1),
                    ],
                    ticktext=["log s", "log 2s", "log k/2", "log k-1"],
                    range=padded_range(np.log(h), np.log(k - 1), 0.01),
                )

            fig.update_yaxes(
                row=row,
                col=col,
                title=dict(
                    text="log δ",
                    font=dict(
                        family="Times New Roman", size=18, color="black"
                    ),
                    standoff=0,
                ),
                color="black",
                gridcolor=GRID_COLOR,
                zeroline=False,
            )

    fig.update_layout(
        paper_bgcolor="white",
        plot_bgcolor="white",
        autosize=False,
        height=HEIGHT,
        width=WIDTH,
        showlegend=False,
        margin=dict(b=5, l=5, r=5, t=5),
    )

    save_figure(fig, outfile)


# -- __main__ -----------------------------------------------------------------


if __name__ == "__main__":
    draw(BREAKPOINT_FUNCTION, "sensitivity.pdf")
