#!/usr/bin/env python3
# pylama:ignore=D103,E402

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Utility to generate figures concerning sequential road networks."""

import os
import sys
import time
from functools import lru_cache
from pathlib import Path

import networkx as nx
import numpy as np
import plotly.io as pio
import similaritymeasures
from more_itertools import pairwise

# The submodules are not packages.
sys.path.append(os.path.join(os.path.dirname(__file__), "webapp"))
sys.path.append(os.path.join(os.path.dirname(__file__), "toolbox"))

from webapp.config import BASE_COST_KEY, COORDS_KEY, IS_SITE_KEY, ROAD_TIER_KEY
from webapp.logic import build_roads, make_network, place_sites

EXTERNAL_DATA_DIR = Path("external_data")

ALPHA_DIGITS = 4  # Prevent numeric/display issues.

UNUSED_COLOR = "#888"
ROAD_COLOR = "#444"
SITE_BORDER_COLOR = ROAD_COLOR
SITE_FILL_COLOR = "orange"  # "#d0e1f0"
GRID_COLOR = "#ccc"


def make_file_names(
    caller_file, *, data_extension=".npy", figure_extension=".pdf"
):
    cwd = Path.cwd()
    stem = Path(caller_file).stem

    data_file = cwd / (stem + data_extension)
    figure_file = cwd / (stem + figure_extension)

    return data_file, figure_file


def run_generate_or_draw(generate_func, draw_func):
    def usage():
        print(f"usage: {sys.argv[0]} (generate|draw)")

    if len(sys.argv) != 2:
        usage()
    elif sys.argv[1] == "generate":
        generate_func()
    elif sys.argv[1] == "draw":
        draw_func()
    else:
        usage()


def save_figure(fig, file):
    pio.write_image(fig, file)

    # HACK: Work around #3469.
    time.sleep(1)
    pio.write_image(fig, file)


def padded_range(a, b, padding=0.02):
    assert a < b
    d = (b - a) * padding
    return a - d, b + d


def make_roadnet(alpha=None, road_seed=1, extract_roads=False, order=None):
    map_class = "delaunay"
    map_resolution = 30
    map_sites = 20
    map_seed = 1

    road_mode = "all_pairs"

    network = make_network(map_class, map_resolution, map_seed)
    network = place_sites(network, map_sites, map_seed)

    if alpha is not None:
        network = build_roads(
            network=network,
            mode=road_mode,
            order=order,
            alpha=alpha,
            seed=road_seed,
            selected_site=None,
        )

    if extract_roads:
        positive_edges = [
            e for e in network.edges if network.edges[e][ROAD_TIER_KEY]
        ]
        return network.edge_subgraph(positive_edges).copy()
    else:
        return network


def _get_sites(network):
    return tuple(sorted(v for v in network if network.nodes[v][IS_SITE_KEY]))


@lru_cache()
def _get_shortest_paths(network):
    sites = _get_sites(network)

    trees = {}
    for u in sites[:-1]:
        trees[u] = nx.single_source_dijkstra_path(
            network, u, weight=BASE_COST_KEY
        )

    paths = {}
    for u, v in pairwise(sites):
        paths[u, v] = np.array(
            [network.nodes[w][COORDS_KEY] for w in trees[u][v]]
        )

    return paths


def compare_networks(G, H):
    G_paths = _get_shortest_paths(G)
    H_paths = _get_shortest_paths(H)

    assert G_paths.keys() == H_paths.keys(), (
        list(G_paths.keys()),
        list(H_paths.keys()),
    )

    error = 0.0

    for P, Q in zip(G_paths.values(), H_paths.values()):
        assert np.all(P[0] == Q[0]) and np.all(P[-1] == Q[-1])

        error += similaritymeasures.area_between_two_curves(P, Q)

    return error / len(G_paths)
