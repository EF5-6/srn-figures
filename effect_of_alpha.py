#!/usr/bin/env python3
# pylama:ignore=D103,E402

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Produce the "effect_of_alpha" figure."""

import plotly.graph_objects as go
from plotly.subplots import make_subplots

from common import (
    ROAD_COLOR,
    SITE_BORDER_COLOR,
    SITE_FILL_COLOR,
    UNUSED_COLOR,
    make_file_names,
    make_roadnet,
    save_figure,
)
from webapp.config import COORDS_KEY, IS_SITE_KEY, ROAD_TIER_KEY
from webapp.draw import _get_edge_coords

_, FIGURE_FILE = make_file_names(__file__)


def draw_network(network, unused=True, roads=True, sites=True):
    traces = []

    if unused:
        if roads:
            unused_coords = _get_edge_coords(
                network, selector=ROAD_TIER_KEY, reverse=True
            )
        else:
            unused_coords = _get_edge_coords(network)

        traces.append(
            go.Scatter(
                mode="lines",
                x=unused_coords[0],
                y=unused_coords[1],
                line=dict(width=0.5, color=UNUSED_COLOR),
            )
        )

    if roads:
        road_coords = _get_edge_coords(network, selector=ROAD_TIER_KEY)

        traces.append(
            go.Scatter(
                mode="lines",
                x=road_coords[0],
                y=road_coords[1],
                line_color=ROAD_COLOR,
                line_width=1.5,
            )
        )

    if sites:
        site_coords = ([], [])

        for _, d in network.nodes(data=True):
            if not d[IS_SITE_KEY]:
                continue

            x, y = d[COORDS_KEY]
            site_coords[0].append(x)
            site_coords[1].append(y)

        traces.append(
            go.Scatter(
                mode="markers",
                x=site_coords[0],
                y=site_coords[1],
                marker=dict(
                    line_width=1.5,
                    line_color=SITE_BORDER_COLOR,
                    color=SITE_FILL_COLOR,
                ),
            )
        )

    return traces


def draw():
    height = 260
    width = 1000
    alphas = [0, 0.4, 0.6, 1]
    arrow_fraction = 0.09
    arrow_margin = 0.015
    arrow_head_length = 0.01
    arrow_text_offset = 0.01
    arrow_text_size = 16.5
    arrow_lightness = 30

    n = len(alphas) + 1
    specs = [
        [{}, {"colspan": n - 1}] + [None] * (n - 2),
        [{} for _ in range(n)],
    ]
    ahl = arrow_head_length
    ato = arrow_head_length + arrow_text_offset

    fig = make_subplots(
        rows=2,
        cols=n,
        vertical_spacing=0,
        horizontal_spacing=0,
        specs=specs,
        row_heights=[arrow_fraction, 1 - arrow_fraction],
    )

    fig.add_trace(
        go.Scatter(
            mode="lines",
            line=dict(color="rgba(0, 0, 0, 0)", width=0),
            fillcolor=f"hsl(0, 0, {arrow_lightness})",
            x=[ahl, 0, ahl, 1 - ahl, 1, 1 - ahl, ahl],
            y=[0, 0.5, 1, 1, 0.5, 0, 0],
            fill="toself",
        ),
        row=1,
        col=2,
    )

    fig.update_xaxes(range=[-arrow_margin, 1 + arrow_margin], row=1, col=2)

    fig.update_yaxes(range=[0, 1], row=1, col=2)

    fig.add_trace(
        go.Scatter(
            mode="text",
            x=[ato, 0.5, 1 - ato],
            y=[0.5] * 3,
            text=[
                "low construction costs",
                "intermediate regime",
                "high mobility benefit",
            ],
            textposition=["middle right", "middle center", "middle left"],
            textfont=dict(
                family="Times New Roman",
                size=arrow_text_size,
                color="white",
            ),
        ),
        row=1,
        col=2,
    )

    for i, alpha in enumerate([None] + alphas):
        r, c = i // n + 2, i % n + 1
        first = alpha is None

        network = make_roadnet(alpha)

        for trace in draw_network(network, unused=first, roads=not first):
            fig.add_trace(trace, row=r, col=c)

        fig.update_xaxes(
            title_text=(
                f"<b>{'ABCDEFGH'[i]}</b>  "
                + ("terrain graph" if first else f"α = {alpha:.1f}")
            ),
            title_font_family="Times New Roman",
            title_font_size=18,
            title_font_color="black",
            title_standoff=0,
            color="black",
            row=r,
            col=c,
        )

        fig.update_yaxes(
            scaleanchor=f"x{i + 3}",
            row=r,
            col=c,
        )

    fig.update_xaxes(
        showgrid=False,
        zeroline=False,
        showticklabels=False,
    )

    fig.update_yaxes(
        showgrid=False,
        zeroline=False,
        showticklabels=False,
    )

    fig.update_layout(
        paper_bgcolor="white",
        plot_bgcolor="white",
        autosize=False,
        height=height,
        width=width,
        showlegend=False,
        margin=dict(b=5, l=5, r=5, t=5),
    )

    save_figure(fig, FIGURE_FILE)


if __name__ == "__main__":
    draw()
