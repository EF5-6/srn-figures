#!/usr/bin/env python3
# pylama:ignore=D103,E402,E741

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Produce the "reconstruction_choice" figure."""

import json
import pickle
from multiprocessing import Pool

import pandas as pd
import plotly.graph_objects as go

from common import EXTERNAL_DATA_DIR, make_file_names, save_figure
from reconstruction import FIGURE_SITE_COLOR, SITE_BORDER_COLOR, ROAD_COLOR
from toolbox.simulation import Simulation

_, FIGURE_FILE = make_file_names(__file__)

FIGURE_HEIGHT = 220
FIGURE_WIDTH = 500

FIGURE_SHADING_COLOR = "rgba(0, 0, 0, 0.05)"
FIGURE_VLINE_COLOR = "rgba(0, 0, 0, 0)"
FIGURE_LINE_COLOR = ROAD_COLOR


def draw():
    with open(EXTERNAL_DATA_DIR / "terrain.pickle", "rb") as f:
        terrain = pickle.load(f)

    with open(EXTERNAL_DATA_DIR / "reference.pickle", "rb") as f:
        reference = pickle.load(f)

    with open(EXTERNAL_DATA_DIR / "evidence.json", "rb") as f:
        evidence = pd.read_json(f)

    sim_reference = Simulation(terrain)
    sim_reference.set_reference(reference)

    sim_evidence = Simulation(terrain)
    sim_evidence.set_evidence(evidence)

    print("Loading plans...")

    plans = []
    for i, plan_file in enumerate(
        (EXTERNAL_DATA_DIR / "plans").glob("*.plan.json")
    ):
        with open(plan_file, "r") as f:
            plan = json.load(f)

        plans.append((tuple(plan["order"]), plan["alpha"]))

    plans = sorted(plans, key=lambda p: p[1])
    alphas = [plan[1] for plan in plans]

    with Pool() as pool:
        print("Computing distances to evidence...")

        distances = pool.starmap(
            sim_evidence.evaluate,
            ((plan, f"tier_{i}", f"cost_{i}") for i, plan in enumerate(plans)),
        )

    print("Plotting results...")

    fig = go.Figure()

    for i, (x0, x1, title) in enumerate(
        zip(
            (0.3, 0.4, 0.5),
            (0.4, 0.5, 0.7),
            ("Underfitted", "Well-matched", "Insufficient evidence"),
        )
    ):
        fig.add_vrect(
            x0=x0,
            x1=x1,
            fillcolor=("rgba(0, 0, 0, 0)" if i != 1 else FIGURE_SHADING_COLOR),
            line_width=0,
            annotation=dict(
                text=title,
                font=dict(color="black", family="Times New Roman", size=14),
            ),
            annotation_position="inside top",
            layer="below",
        )

    vline_args = dict(
        layer="below", line=dict(dash="dot", color=FIGURE_VLINE_COLOR)
    )

    fig.add_vline(x=0.4, **vline_args)
    fig.add_vline(x=0.5, **vline_args)

    fig.add_trace(
        go.Scatter(
            name="Δ evidence",
            mode="lines+markers",
            x=alphas,
            y=[d / 1e6 for d in distances],
            line_color=FIGURE_LINE_COLOR,
            marker_color=FIGURE_LINE_COLOR,
        )
    )

    # Add explanatory traces.
    for i, x, y in zip(
        range(2, 6),
        (
            [0, 0, 1, 1],
            [0, 0, 1, 1, 0],
            [0, 0, 1, 1, 0, 1],
            [0, 0, 1, 1, 0, 1, None, 1, 0],
        ),
        (
            [1, 0, 0, 1],
            [0, 1, 1, 0, 0],
            [0, 1, 1, 0, 0, 1],
            [0, 1, 1, 0, 0, 1, None, 0, 1],
        ),
    ):
        fig.add_trace(
            go.Scatter(
                mode="lines",
                x=x,
                y=y,
                xaxis=f"x{i}",
                yaxis=f"y{i}",
                showlegend=False,
                line=dict(width=2, color=ROAD_COLOR),
            )
        )
        fig.add_trace(
            go.Scatter(
                mode="markers",
                x=[0, 0, 1, 1],
                y=[1, 0, 0, 1],
                xaxis=f"x{i}",
                yaxis=f"y{i}",
                showlegend=False,
                marker=dict(
                    size=8,
                    line_width=1.5,
                    line_color=SITE_BORDER_COLOR,
                    color=FIGURE_SITE_COLOR,
                ),
            )
        )
        fig.add_trace(
            go.Scatter(
                mode="markers",
                x=[0.5, 0.5, 0.0, 1.0],
                y=[0.0, 1.0, 0.5, 0.5],
                xaxis=f"x{i}",
                yaxis=f"y{i}",
                showlegend=False,
                marker=dict(
                    symbol=2,
                    size=6.5,
                    line_width=1.5,
                    line_color=SITE_BORDER_COLOR,
                    color="rgb(255, 100, 100)",
                ),
            )
        )

    inset_args = dict(
        showgrid=False,
        zeroline=False,
        showticklabels=False,
    )

    fig.update_layout(
        autosize=False,
        height=FIGURE_HEIGHT,
        width=FIGURE_WIDTH,
        margin=dict(b=5, l=5, r=5, t=5),
        showlegend=False,
        paper_bgcolor="white",
        plot_bgcolor="rgba(0, 0, 0, 0)",
        xaxis=dict(
            title=dict(
                text="Parameter α",
                font=dict(family="Times New Roman", size=18, color="black"),
            ),
            dtick=0.05,
            tickformat=".2f",
            showgrid=False,
        ),
        yaxis=dict(
            title=dict(
                text="Squared distances<br />to evidence (10⁶ m²)",
                font=dict(family="Times New Roman", size=18, color="black"),
            ),
            dtick=2,
            showgrid=False,
        ),
        xaxis2=dict(domain=(0.095, 0.245), **inset_args),
        xaxis3=dict(domain=(0.315, 0.465), **inset_args),
        xaxis4=dict(domain=(0.54, 0.69), **inset_args),
        xaxis5=dict(domain=(0.69, 0.94), **inset_args),
        yaxis2=dict(domain=(0.1, 0.4), scaleanchor="x2", **inset_args),
        yaxis3=dict(domain=(0.5, 0.8), scaleanchor="x3", **inset_args),
        yaxis4=dict(domain=(0.5, 0.8), scaleanchor="x4", **inset_args),
        yaxis5=dict(domain=(0.5, 0.8), scaleanchor="x5", **inset_args),
    )

    save_figure(fig, FIGURE_FILE)


if __name__ == "__main__":
    draw()
