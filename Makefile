all: .check_deps effect_of_alpha.pdf path_dependence.pdf breakpoint.pdf \
     permutahedron.pdf transpositions.pdf reconstruction.pdf \
     reconstruction_choice.pdf reconstruction_more.pdf model_comparison.pdf

quick: precomputed all

extra: sensitivity.pdf

mostlyclean:
	rm -f *.pdf

precomputed:
	cp -v ./precomputed_data/*.npy .

.check_deps:
	@echo "Checking Python depdendencies..."
	@./check_deps.py
	@echo "All necessary Python dependencies found!"
	@touch .check_deps

breakpoint.npy:
	./breakpoint.py generate

breakpoint.pdf: breakpoint.npy
	./breakpoint.py draw

effect_of_alpha.pdf:
	./effect_of_alpha.py

path_dependence.npy:
	./path_dependence.py generate

model_comparison.npy:
	./model_comparison.py generate

path_dependence.pdf: path_dependence.npy
	./path_dependence.py draw

# Dependency on breakpoint.npy intentional.
permutahedron.pdf: breakpoint.npy
	./permutahedron.py

transpositions.npy:
	./transpositions.py generate

# Dependency on breakpoint.npy intentional.
transpositions.pdf: transpositions.npy breakpoint.npy
	./transpositions.py draw

reconstruction.pdf:
	./reconstruction.py

reconstruction_choice.pdf:
	./reconstruction_choice.py

reconstruction_more.pdf:
	./reconstruction_more.py

model_comparison.pdf: model_comparison.npy
	./model_comparison.py draw

sensitivity.pdf:
	./sensitivity.py
