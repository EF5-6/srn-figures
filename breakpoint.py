#!/usr/bin/env python3
# pylama:ignore=D103,E203,E402,E741

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Produce the "breakpoint" and "breakpoint_extended" figures."""

# HACK: The split into a basic and extended figure was performed ad-hoc.
# TODO: Separate the code for both figures more cleanly.

from multiprocessing import Pool

import scipy.stats as ss
import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from common import (
    GRID_COLOR,
    compare_networks,
    make_file_names,
    make_roadnet,
    padded_range,
    run_generate_or_draw,
    save_figure,
)
from toolbox.comprep import CompositionalRepresentation
from webapp.config import IS_SITE_KEY

DATA_FILE, FIGURE_FILE_BASE = make_file_names(__file__)

DATA_ALPHAS = [0.75, 0.5, 0.0]
DATA_NUM_NETS = 10000

FIGURE_HEIGHT = 300
FIGURE_WIDTH = 500
EXTENDED_FIGURE_HEIGHT = 600
EXTENDED_FIGURE_WIDTH = 600
FIGURE_COLORS = ["orange", "red", "blue"]
FIGURE_SYMBOLS = [102, 101, 100]


def _make_and_compare(base_network, alpha, seed, order):
    network = make_roadnet(alpha, seed, extract_roads=True, order=tuple(order))

    d = compare_networks(base_network, network)

    return d


def generate():
    # HACK: Get number of sites and connections.
    probe_network = make_roadnet(0, 1)
    probe_sites = tuple(
        v for v, d in probe_network.nodes(data=True) if d[IS_SITE_KEY]
    )
    s = len(probe_sites)
    k = s * (s - 1) // 2

    np.random.seed(1)

    alphas = np.array(DATA_ALPHAS)
    D = np.zeros((0, k - 1))

    for alpha in alphas:
        print(
            f"  Computing {DATA_NUM_NETS} base networks for alpha = "
            f"{alpha:.2f}..."
        )

        seeds = np.random.randint(2**32 - 1, size=DATA_NUM_NETS)

        # Create DATA_NUM_NETS many random base orders.
        base_orders = []
        for seed in seeds:
            np.random.seed(seed)
            base_order = list(range(k))
            np.random.shuffle(base_order)
            base_orders.append(base_order)

        # Compute a base network from every base order.
        with Pool() as pool:
            base_networks = pool.starmap(
                make_roadnet,
                (
                    (alpha, None, True, tuple(base_order))
                    for base_order in base_orders
                ),
            )

        # For every base order, compare all adjacent transpositions with
        # the associated base network.
        d = np.zeros(k - 1)
        for i in range(len(seeds)):
            base_order = base_orders[i]
            base_network = base_networks[i]

            print(
                f"  Evaluating {k - 1} variations of base "
                f"network {i + 1}/{DATA_NUM_NETS} for alpha = {alpha:.2f}..."
            )

            orders = [tuple(base_order)]  # Base to find numeric zero.
            for i in range(k - 1):
                order = base_order.copy()
                order[i], order[i + 1] = order[i + 1], order[i]
                orders.append(tuple(order))

            with Pool() as pool:
                results = pool.starmap(
                    _make_and_compare,
                    ((base_network, alpha, seed, order) for order in orders),
                )

            zero = results.pop(0)

            # Clip at zero to work around rare numeric issues.
            d += (np.array(results) - zero).clip(0)

        d /= DATA_NUM_NETS
        D = np.vstack([D, d[np.newaxis, :]])

    np.save(DATA_FILE, np.hstack([alphas[:, np.newaxis], D]))


def draw():
    _draw(extended=False)
    _draw(extended=True)


def _draw(extended):
    if not DATA_FILE.exists():
        raise RuntimeError(f"Missing data file {DATA_FILE}.")

    # Load data.
    A = np.load(DATA_FILE)
    alphas = A[:, 0]  # First column.
    D = A[:, 1:]  # Remaining columns.

    # Recover metadata.
    k = D.shape[1] + 1
    s = int((1 + 8 * k) ** 0.5 + 1) // 2
    assert k == s * (s - 1) // 2
    I = np.array(range(1, k))

    # Normalize dissimilarities.
    D /= np.max(D)

    # Prepare figure.
    if extended:
        fig = make_subplots(rows=3)

        # Add subfigure letters.
        for letter, y in zip("ABC", (0.992, 0.605, 0.218)):
            fig.add_annotation(
                text=f"<b>{letter}</b>",
                xref="paper",
                yref="paper",
                x=0.98,
                y=y,
                showarrow=False,
                font=dict(family="Times New Roman", size=18, color="black"),
            )
    else:
        fig = go.Figure()

    # Add traces.
    for i, alpha in enumerate(alphas):
        d = D[i, :]
        nz = np.nonzero(d)
        x = I[nz]
        y = d[nz]

        if alpha == 0:
            # For alpha = 1, the relationship appears to be exponential.

            # NOTE: We still fit the exponential to the first s samples only as
            #       later samples are noisy at this sample size.
            _d = d[:s]
            _nz = np.nonzero(_d)
            _x = I[:s][_nz]
            _y = _d[_nz]

            a, b = ss.linregress(_x, np.log(_y))[:2]
            fit = np.exp(a * x + b)
        else:
            # For other alpha, fit an exponential/power two-piece function with
            # its breakpoint at s.
            params = CompositionalRepresentation.from_stats_vector(
                D[i, :], return_params=True, breakpoint=s
            )

            _, h, beta, lbd, delta, gamma = params
            xe = x[:h]
            xp = x[h:]
            fe = beta * np.exp(lbd * xe)
            fp = delta * xp**gamma
            fit = np.concatenate([fe, fp])

        marker = dict(color=FIGURE_COLORS[i], symbol=FIGURE_SYMBOLS[i], size=5)

        if extended:
            # Draw A.
            linear = go.Scatter(
                x=x,
                y=y,
                mode="markers",
                marker=marker,
                name=f"{alpha:.2f}",
            )
            linear_fit = go.Scatter(
                x=x,
                y=fit,
                mode="lines",
                line_color=FIGURE_COLORS[i],
                showlegend=False,
            )

        # Draw B.
        logarithmic = go.Scatter(
            x=x[:s] if extended else x,
            y=np.log(y[:s]) if extended else np.log(y),
            mode="markers",
            marker=marker,
            name=f"{alpha:.2f}",
            showlegend=not extended,
        )
        logarithmic_fit = go.Scatter(
            x=x[:s] if extended else x,
            y=np.log(fit[:s]) if extended else np.log(fit),
            mode="lines",
            line_color=FIGURE_COLORS[i],
            showlegend=False,
        )

        if extended:
            # Draw C.
            loglog = go.Scatter(
                x=np.log(x[s - 1 :]),
                y=np.log(y[s - 1 :]),
                mode="markers",
                marker=marker,
                showlegend=False,
            )
            loglog_fit = go.Scatter(
                x=np.log(x[s - 1 :]),
                y=np.log(fit[s - 1 :]),
                mode="lines",
                line_color=FIGURE_COLORS[i],
                showlegend=False,
            )

        if extended:
            # Draw inset on A.
            linear_inset = go.Scatter(
                x=x,
                y=y,
                mode="markers",
                marker=marker,
                name=f"{alpha:.2f}",
                xaxis="x4",
                yaxis="y4",
                showlegend=False,
            )
            linear_fit_inset = go.Scatter(
                x=x,
                y=fit,
                mode="lines",
                line_color=FIGURE_COLORS[i],
                showlegend=False,
                xaxis="x4",
                yaxis="y4",
            )

        if not extended:
            # Draw inset on B.
            logarithmic_inset = go.Scatter(
                x=x,
                y=np.log(y),
                mode="markers",
                marker=marker,
                name=f"{alpha:.2f}",
                xaxis="x5",
                yaxis="y5",
                showlegend=False,
            )
            logarithmic_fit_inset = go.Scatter(
                x=x,
                y=np.log(fit),
                mode="lines",
                line_color=FIGURE_COLORS[i],
                showlegend=False,
                xaxis="x5",
                yaxis="y5",
            )

        # Add traces to figure.
        if extended:
            fig.add_trace(linear, col=1, row=1)
            fig.add_trace(linear_fit, col=1, row=1)
            fig.add_trace(logarithmic, col=1, row=2)
            fig.add_trace(logarithmic_fit, col=1, row=2)
            fig.add_trace(loglog, col=1, row=3)
            fig.add_trace(loglog_fit, col=1, row=3)
            fig.add_trace(linear_inset)
            fig.add_trace(linear_fit_inset)
        else:
            fig.add_trace(logarithmic)
            fig.add_trace(logarithmic_fit)
            fig.add_trace(logarithmic_inset)
            fig.add_trace(logarithmic_fit_inset)

    # Configure x axes.
    fig.update_xaxes(
        gridcolor=GRID_COLOR,
        tickmode="array",
        title_font_family="Times New Roman",
        title_font_size=18,
        title_font_color="black",
        title_standoff=12,
        color="black",
        zeroline=False,
    )

    if extended:
        fig.update_xaxes(
            col=1,
            row=1,
            title_text=(
                "Index i of an (i, i + 1)-exchange in a connection order π"
            ),
            tickvals=[1, s, k - 1],
            ticktext=[1, "s", "k-1"],
            range=padded_range(1, k - 1, padding=0.01),
            title_standoff=0,
        )
        fig.update_xaxes(
            col=1,
            row=2,
            title_text="i",
            tickvals=[1, s, k - 1],
            ticktext=[1, "s", "k-1"],
            range=padded_range(1, s, padding=0.01),
            title_standoff=0,
        )
        fig.update_xaxes(
            col=1,
            row=3,
            title_text="log i",
            tickvals=[0, np.log(s), np.log(k)],
            ticktext=[0, "log s", "log k"],
            range=padded_range(np.log(s), np.log(k), padding=0.01),
            title_standoff=0,
        )
    else:
        fig.update_xaxes(
            title_text=(
                "Index i of an (i, i + 1)-exchange in a connection order π"
            ),
            tickvals=[1, s, k - 1],
            ticktext=[1, "s", "k-1"],
            range=padded_range(1, k - 1),
        )

    # Configure y axes.
    fig.update_yaxes(
        gridcolor=GRID_COLOR,
        title_font_family="Times New Roman",
        title_font_size=18,
        title_font_color="black",
        color="black",
        zeroline=False,
    )

    if extended:
        fig.update_yaxes(
            col=1,
            row=1,
            title_text="Mean netw. change δ",
            tickvals=[0, 0.2, 1],
            range=padded_range(0, 1),
        )
        fig.update_yaxes(
            col=1,
            row=2,
            title_text="log δ",
            tickvals=[-7, -5, 0],
            range=padded_range(-7, 0),
            title_standoff=0,
        )
        fig.update_yaxes(
            col=1,
            row=3,
            title_text="log δ",
            title_standoff=0,
            tickvals=[-16, -7, -5],
            range=padded_range(-16, -5),
        )
    else:
        fig.update_yaxes(
            title_text="Mean network change (logarithmic)",
            tickvals=[-16, -7, 0],
            range=padded_range(-16, 0),
        )

    # Shade the insets and their reference regions.
    if extended:
        insets = [((1, 4), 0.2)]
    else:
        insets = [((1, 5), -7)]

    inset_shading = [
        dict(
            type="rect",
            xref=f"x{axis}",
            yref=f"y{axis}",
            x0=1,
            y0=0,
            x1=s,
            y1=y1,
            fillcolor="rgba(0, 0, 0, 0.05)",
            layer="below",
            line_width=0,
        )
        for axes, y1 in insets
        for axis in axes
    ]

    # Configure layout.
    fig.update_layout(
        autosize=False,
        height=EXTENDED_FIGURE_HEIGHT if extended else FIGURE_HEIGHT,
        width=EXTENDED_FIGURE_WIDTH if extended else FIGURE_WIDTH,
        margin=dict(b=5, l=5, r=5, t=5),
        paper_bgcolor="white",
        plot_bgcolor="white",
        legend=dict(
            x=0.775 if extended else 0.790,
            y=0.990 if extended else 0.968,
            title=dict(
                text="Parameter α",
                font=dict(
                    family="Times New Roman",
                    color="black",
                    size=16 if extended else None,
                ),
            ),
            font=dict(
                family="Times New Roman",
                color="black",
                size=14 if extended else None,
            ),
            bgcolor="rgba(0, 0, 0, 0)",
        ),
        shapes=inset_shading,
    )

    if extended:
        fig.update_layout(
            xaxis4=dict(
                gridcolor=GRID_COLOR,
                range=padded_range(1, s),
                tickvals=[1, s],
                ticktext=[1, "s"],
                domain=[0.25, 0.75],
                anchor="y4",
                zeroline=False,
            ),
            yaxis4=dict(
                gridcolor=GRID_COLOR,
                range=padded_range(0, 0.2, padding=0.04),
                tickvals=[0, 0.2],
                domain=[0.83, 0.98],
                anchor="x4",
                zeroline=False,
            ),
        )
    else:
        fig.update_layout(
            xaxis5=dict(
                gridcolor=GRID_COLOR,
                range=padded_range(1, s),
                tickvals=[1, s],
                ticktext=[1, "s"],
                domain=[0.25, 0.75],
                anchor="y5",
                zeroline=False,
            ),
            yaxis5=dict(
                gridcolor=GRID_COLOR,
                range=padded_range(-7, 0, padding=0.05),
                tickvals=[-7, 0],
                domain=[0.65, 0.95],
                anchor="x5",
                zeroline=False,
            ),
        )

    if extended:
        figure_file = FIGURE_FILE_BASE.with_stem(
            FIGURE_FILE_BASE.stem + "_extended"
        )
    else:
        figure_file = FIGURE_FILE_BASE

    print(f"Saving {figure_file.name}...")

    save_figure(fig, figure_file)


if __name__ == "__main__":
    run_generate_or_draw(generate, draw)
