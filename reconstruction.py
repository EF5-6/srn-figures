#!/usr/bin/env python3
# pylama:ignore=D103,E402,E741

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Produce the "reconstruction" figure."""

import json
import pickle
from copy import copy
from functools import lru_cache

import plotly.graph_objects as go
from PIL import Image
from plotly.subplots import make_subplots

from common import (
    COORDS_KEY,
    EXTERNAL_DATA_DIR,
    IS_SITE_KEY,
    ROAD_COLOR,
    ROAD_TIER_KEY,
    SITE_BORDER_COLOR,
    UNUSED_COLOR,
    make_file_names,
    save_figure,
)
from webapp.logic import build_roads

VERTICES_KEY = "_vertices"

_, FIGURE_FILE = make_file_names(__file__)

FIGURE_HEIGHT = 570
FIGURE_WIDTH = 1000

FIGURE_SITE_COLOR = "lightblue"
FIGURE_TERRAIN_COLOR = UNUSED_COLOR
FIGURE_TERRAIN_WIDTH = 1
FIGURE_REFERENCE_COLOR = "rgb(255, 200, 0)"
FIGURE_REFERENCE_WIDTH = 3


@lru_cache()
def get_edge_coords(network, selector=None, *, reverse=False):
    x_seq, y_seq = [], []

    for _, _, d in network.edges(data=True):
        if selector and bool(d[selector]) is bool(reverse):
            continue

        vertices = d[VERTICES_KEY]
        x, y = zip(*vertices)

        x_seq.extend(x)
        y_seq.extend(y)

        x_seq.append(None)
        y_seq.append(None)

    return x_seq, y_seq


def draw_terrain(network, *, legend=False, color=ROAD_COLOR):
    x, y = get_edge_coords(network)

    return go.Scatter(
        name="terrain graph",
        showlegend=legend,
        mode="lines",
        x=x,
        y=y,
        line=dict(width=FIGURE_TERRAIN_WIDTH, color=color),
    )


def draw_reference(network, legend=True):
    x, y = get_edge_coords(network)

    return go.Scatter(
        name="reference<br />network",
        legendrank=200,
        showlegend=legend,
        mode="lines",
        x=x,
        y=y,
        line=dict(width=FIGURE_REFERENCE_WIDTH, color=FIGURE_REFERENCE_COLOR),
    )


def draw_roads(network, legend=True, use_road_tier_key=True):
    x, y = get_edge_coords(
        network, ROAD_TIER_KEY if use_road_tier_key else None
    )

    return go.Scatter(
        name="predicted<br />network",
        legendrank=100,
        showlegend=legend,
        mode="lines",
        x=x,
        y=y,
        line=dict(width=2, color=ROAD_COLOR),
    )


@lru_cache()
def _sites_data(network):
    x, y, names = [], [], []

    for _, d in network.nodes(data=True):
        if not d[IS_SITE_KEY]:
            continue

        x.append(d[COORDS_KEY][0])
        y.append(d[COORDS_KEY][1])

        name = d["site_name"]

        # HACK: Reformat names.
        name = name.replace("V", "U").title().replace("C(Astra)", "C(astra)")

        names.append(name)

    return x, y, names


def draw_sites(network):
    x, y, _ = _sites_data(network)

    return go.Scatter(
        name="site",
        legendrank=10,
        mode="markers",
        x=x,
        y=y,
        marker=dict(
            size=8,
            line_width=1.5,
            line_color=SITE_BORDER_COLOR,
            color=FIGURE_SITE_COLOR,
        ),
    )


def draw_stations(stations):
    coords = stations[COORDS_KEY].values()
    x, y = zip(*coords)

    return go.Scatter(
        name="station",
        legendrank=20,
        mode="markers",
        x=x,
        y=y,
        marker=dict(
            symbol=2,
            size=6.5,
            line_width=1.5,
            line_color=SITE_BORDER_COLOR,
            color="rgb(255, 100, 100)",
        ),
    )


def draw_milestones(milestones):
    coords = milestones[COORDS_KEY].values()
    x, y = zip(*coords)

    return go.Scatter(
        name="milestone",
        legendrank=25,
        mode="markers",
        x=x,
        y=y,
        marker=dict(
            symbol=5,
            size=6.5,
            line_width=1.5,
            line_color=SITE_BORDER_COLOR,
            color="rgb(255, 255, 100)",
        ),
    )


def annotate_sites(network):
    x, y, names = _sites_data(network)

    h = 12.5

    # HACK: Manually reposition some labels.
    offsets = {name: (0, -h) for name in names}
    offsets["Ad Herculem"] = (15, -h)
    offsets["Portus Tibulas"] = (0, h)
    offsets["Othoca"] = (20, -h)
    offsets["Tharros"] = (-20, -h)

    for xi, yi, name in zip(x, y, names):
        dx, dy = offsets[name]

        yield dict(
            x=xi,
            y=yi,
            text=name,
            showarrow=False,
            font=dict(family="Times New Roman", size=11.5, color="black"),
            xshift=dx,
            yshift=dy,
            bgcolor="white",
            opacity=0.8,
        )


def draw():
    # Load external data.
    with open(EXTERNAL_DATA_DIR / "terrain.pickle", "rb") as f:
        terrain = pickle.load(f)

    with open(EXTERNAL_DATA_DIR / "reference.pickle", "rb") as f:
        reference = pickle.load(f)

    elevation = Image.open(EXTERNAL_DATA_DIR / "elevation.png")
    outline = Image.open(EXTERNAL_DATA_DIR / "outline.png")

    with open(EXTERNAL_DATA_DIR / "elevation.json") as f:
        elevation_data = json.load(f)

    with open(EXTERNAL_DATA_DIR / "plans/0.40_1.6084e+07.plan.json", "r") as f:
        plan = json.load(f)
        first_alpha = plan["alpha"]
        first_order = tuple(plan["order"])

    with open(EXTERNAL_DATA_DIR / "plans/0.50_1.5297e+07.plan.json", "r") as f:
        plan = json.load(f)
        second_alpha = plan["alpha"]
        second_order = tuple(plan["order"])

    with open(EXTERNAL_DATA_DIR / "stations.json") as f:
        stations = json.load(f)

    with open(EXTERNAL_DATA_DIR / "milestones.json") as f:
        milestones = json.load(f)

    # Evaluate plans.
    first_reconstruction = build_roads(
        network=terrain,
        mode="all_pairs",
        alpha=first_alpha,
        order=first_order,
        seed=None,
        selected_site=None,
    )
    second_reconstruction = build_roads(
        network=terrain,
        mode="all_pairs",
        alpha=second_alpha,
        order=second_order,
        seed=None,
        selected_site=None,
    )

    # Create shared traces.
    sites_trace = draw_sites(terrain)
    stations_trace = draw_stations(stations)
    milestones_trace = draw_milestones(milestones)

    # Setup a figure.
    fig = make_subplots(rows=1, cols=3, horizontal_spacing=0.04)

    # Assemble first subfigure.
    fig.add_trace(draw_terrain(terrain), row=1, col=1)
    fig.add_trace(stations_trace, row=1, col=1)
    fig.add_trace(sites_trace, row=1, col=1)
    for annotation in annotate_sites(terrain):
        fig.add_annotation(annotation, row=1, col=1)

    # Avoid duplicate legend entries.
    sites_trace = copy(sites_trace)
    stations_trace = copy(stations_trace)
    sites_trace["showlegend"] = False
    stations_trace["showlegend"] = False

    # Assemble second subfigure.
    fig.add_trace(draw_reference(reference), row=1, col=2)
    fig.add_trace(draw_roads(first_reconstruction), row=1, col=2)
    fig.add_trace(milestones_trace, row=1, col=2)
    fig.add_trace(stations_trace, row=1, col=2)
    fig.add_trace(sites_trace, row=1, col=2)

    # Avoid duplicate legend entries.
    milestones_trace = copy(milestones_trace)
    milestones_trace["showlegend"] = False

    # Assemble third subfigure.
    fig.add_trace(draw_reference(reference, False), row=1, col=3)
    fig.add_trace(draw_roads(second_reconstruction, False), row=1, col=3)
    fig.add_trace(milestones_trace, row=1, col=3)
    fig.add_trace(stations_trace, row=1, col=3)
    fig.add_trace(sites_trace, row=1, col=3)

    # Add background images.
    for i in range(3):
        fig.add_layout_image(
            source=outline if i else elevation,
            xref=f"x{i + 1}",
            yref=f"y{i + 1}",
            x=elevation_data["west"],
            y=elevation_data["south"],
            sizex=elevation_data["ewres"] * elevation_data["cols"],
            sizey=elevation_data["nsres"] * elevation_data["rows"],
            xanchor="left",
            yanchor="bottom",
            layer="below",
            opacity=0.5 if i else 1,
        )

    # HACK: Add a colorbar for the background image.
    colorbar_trace = go.Scatter(
        showlegend=False,
        x=[None],
        y=[None],
        mode="markers",
        marker=dict(
            colorscale=[
                [(-200 + 200) / 2200, "rgb(110, 174, 161)"],
                [(0 + 200) / 2200, "rgb(143, 188, 143)"],
                [(600 + 200) / 2200, "rgb(255, 255, 0)"],
                [(1200 + 200) / 2200, "rgb(255, 165, 0)"],
                [(2000 + 200) / 2200, "rgb(198, 95, 71)"],
            ],
            showscale=True,
            cmin=-0.2,
            cmax=2.0,
            colorbar=dict(
                title="elevation",
                title_font=dict(
                    family="Times New Roman", size=16, color="black"
                ),
                tickvals=[0.0, 0.6, 1.2, 1.8],
                tickfont=dict(
                    family="Times New Roman", size=14, color="black"
                ),
                tickformat=".1f",
                ticksuffix=" km",
                ticks="inside",
                thickness=14,
                outlinewidth=2,
                tickwidth=2,
                len=0.38,
                x=0.29,
                y=0.42,
            ),
        ),
    )
    fig.add_trace(colorbar_trace)

    # Set the east-west axes.
    labels = [
        "<b>A</b>  DEM and terrain graph",
        f"<b>B</b>  Reconstruction for α = {first_alpha}",
        f"<b>C</b>  Reconstruction for α = {second_alpha}",
    ]
    for i in range(3):
        fig.update_xaxes(
            showgrid=False,
            zeroline=False,
            showticklabels=False,
            range=(elevation_data["west"], elevation_data["east"]),
            title=dict(
                text=labels[i],
                font=dict(family="Times New Roman", size=18, color="black"),
                standoff=0,
            ),
            row=1,
            col=i + 1,
        )

    # Set the north-south axes.
    for i in range(3):
        fig.update_yaxes(
            showgrid=False,
            zeroline=False,
            showticklabels=False,
            range=(elevation_data["south"], elevation_data["north"]),
            scaleanchor=f"x{i + 1}",
            row=1,
            col=i + 1,
        )

    # Finish the layout.
    fig.update_layout(
        showlegend=True,
        autosize=False,
        height=FIGURE_HEIGHT,
        width=FIGURE_WIDTH,
        margin=dict(b=5, l=5, r=5, t=5),
        paper_bgcolor="white",
        plot_bgcolor="white",
        legend=dict(
            font=dict(family="Times New Roman", size=14, color="black"),
            bgcolor="rgba(0, 0, 0, 0)",
            x=0.63,
            y=0.41,
        ),
    )

    save_figure(fig, FIGURE_FILE)


if __name__ == "__main__":
    draw()
